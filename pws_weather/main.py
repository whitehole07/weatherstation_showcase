from time import time
from requests import get as GET
from requests.exceptions import ConnectionError as RequestConnectionError
from requests.exceptions import ReadTimeout

from config import GLOBAL_PATH
from configs.pws_config import PWS_STATION_ID, PWS_STATION_APIKEY, UPLOAD_INTERVAL
from utilities.unit_conversion import UnitConversion
from utilities.decorator_error_handler import retry


class PWS_Weather(object):
    last_upload: float = 0.0

    @classmethod
    def pws_upload(cls, **kwargs):
        if time() - cls.last_upload >= UPLOAD_INTERVAL:
            cls._pws_upload(**kwargs)
            cls.last_upload = time()

    @classmethod
    @retry(BaseException, tries=3, delay=0.5, jitter=1, log_path=GLOBAL_PATH)
    def _pws_upload(cls, **kwargs) -> None:
        """Weather Data Uploads on PWS Weather PWS Network"""
        PWSurl: str = "https://www.pwsweather.com/pwsupdate/pwsupdate.php?"
        PWScreds: str = "ID=" + PWS_STATION_ID + "&PASSWORD=" + PWS_STATION_APIKEY
        date_str: str = "&dateutc=now"
        action_str: str = "&action=updateraw&realtime=1&rtfreq=1"

        try:
            GET(
                PWSurl +
                PWScreds +
                date_str +
                f"&humidity={kwargs['humidity_mean']}"
                f"&baromin={UnitConversion.hpa_to_inches(kwargs['pressure_mean'])}"
                f"&tempf={UnitConversion.celsius_to_farenheit(kwargs['temperature_mean'])}"
                f"&dewptf={UnitConversion.celsius_to_farenheit(kwargs['dew_point'])}"
                f"&dailyrainin={UnitConversion.mm_to_inches(kwargs['rain'])}"
                f"&rainin={UnitConversion.mm_to_inches(kwargs['rain_rate'])}"
                f"&windspeedmph={UnitConversion.kph_to_mph(kwargs['wind'])}"
                f"&winddir={kwargs['wind_dir_degree']}"
                f"&windgustmph={UnitConversion.kph_to_mph(kwargs['wind_gust'])}"
                f"&solarradiation={kwargs['solar_radiation']}"
                f"&uv={kwargs['uv_index']}" +
                action_str,
                timeout=10)
        except (RequestConnectionError, ReadTimeout):
            pass

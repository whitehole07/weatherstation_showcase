__all__ = ["MeteoGram"]

from datetime import datetime, timedelta
from requests import get as GET
from requests import ReadTimeout
from requests import ConnectionError as RequestConnectionError
from urllib.parse import quote
from json import dumps

from config import GLOBAL_PATH
from utilities.imageBB import ImageBB
from jsons.json_utilities import JSONUtilities
from utilities.decorator_error_handler import retry


class MeteoGram(object):
    current_request: str = ""
    last_api_request: float = 0.0

    # OPTIONS
    current_options: dict = JSONUtilities.load_json("meteogram.json")

    @classmethod
    def meteogram_request(cls, force: bool = False) -> str:
        """Get current meteogram"""
        cls._meteogram_request(force)

        return cls.current_request

    @classmethod
    @retry(BaseException, tries=1, log_path=GLOBAL_PATH)
    def _meteogram_request(cls, force: bool = False) -> None:
        """Get current meteogram"""
        if (datetime.now().timestamp() - cls.last_api_request) >= ((24 * 60 * 60) / cls.current_options["PER_DAY_API_CURRENT_REQUESTS"]) or force:
            cls.last_api_request = datetime.now().timestamp()
            if cls.current_options["active"] == "true":
                if current_request := ImageBB.upload_image(cls._get_meteogram(), expiration=cls.current_options["mg_expiration"]):  # Images will expire in one 4h
                    cls.current_request = current_request

    @classmethod
    @retry((RequestConnectionError, ReadTimeout), tries=2, delay=0.5, jitter=1, log_path=GLOBAL_PATH)
    def _get_meteogram(cls) -> bytes:
        r = GET(cls.get_pure_url(), timeout=60)
        if r.status_code != 200:
            raise RequestConnectionError(f"Status code: {r.status_code}, URL: {r.url}, Text: {r.reason} || {r.text} || {r.content}")
        return r.content

    @classmethod
    def get_next_update(cls) -> str:
        return cls._get_next_update_datetime().strftime("%A at %H:%M")

    @classmethod
    def _get_next_update_datetime(cls) -> datetime:
        return datetime.fromtimestamp(cls.last_api_request) + timedelta(seconds=((24 * 60 * 60) / cls.current_options["PER_DAY_API_CURRENT_REQUESTS"]))

    @classmethod
    def get_pure_url(cls) -> str:
        return "https://nodeserver.cloud3squared.com/getMeteogram/" + quote(dumps(cls.current_options["mg_options"]))

    @classmethod
    def get_options(cls) -> list:
        return [
            [0, "Active", cls.current_options["active"]],
            [1, "Request per day", cls.current_options["PER_DAY_API_CURRENT_REQUESTS"]],
            [2, "Image Expiration", f'{cls.current_options["mg_expiration"]}s'],
            [3, "Provider", cls.current_options["mg_options"]["provider"][""]],
            [4, "Width", cls.current_options["mg_options"]["chartWidth"] + "px"],
            [5, "Height", cls.current_options["mg_options"]["chartHeight"] + "px"],
            [6, "Latitude", cls.current_options["mg_options"]["latitude"]],
            [7, "Longitude", cls.current_options["mg_options"]["longitude"]],
            [8, "Full days [TR: true=day, false=hour]", cls.current_options["mg_options"]["days"]["full"]],
            [9, "Time Range [hour]", f'{cls.current_options["mg_options"]["timeRange"]["hours"][0]}h to {cls.current_options["mg_options"]["timeRange"]["hours"][1]}h'],
            [10, "Time Range [day]", f'{cls.current_options["mg_options"]["timeRange"]["days"][0]}g to {cls.current_options["mg_options"]["timeRange"]["days"][1]}g'],
            [11, "Time Machine", cls.current_options["mg_options"]["timeMachine"][""]],
            [12, "Date Time Machine", cls.current_options["mg_options"]["timeMachine"]["date"]],
            [13, "Pressure", cls.current_options["mg_options"]["pressure"][""]],
            [14, "Dew Point", cls.current_options["mg_options"]["dewpoint"][""]],
            [15, "Precipitation", cls.current_options["mg_options"]["precipitation"][""]],
            [16, "Snow Precipitation", cls.current_options["mg_options"]["precipitationSnow"][""]],
            [17, "GFS", cls.current_options["mg_options"]["gfs"][""]],
            [18, "Weather Bar", cls.current_options["mg_options"]["weatherBar"][""]],
            [20, "Cloud Layers", cls.current_options["mg_options"]["cloudLayers"][""]],
            [21, "Header:Max Temperature", cls.current_options["mg_options"]["header"]["temperature"]["max"]],
            [22, "Header:Min Temperature", cls.current_options["mg_options"]["header"]["temperature"]["min"]],
            [23, "Header:Summary", cls.current_options["mg_options"]["header"]["summary"][""]],
            [24, "Header:Alerts", cls.current_options["mg_options"]["header"]["alerts"][""]],
            [25, "Header:Provider", cls.current_options["mg_options"]["header"]["provider"][""]],
            [26, "Header:Model Update Time", cls.current_options["mg_options"]["header"]["modelUpdateTime"][""]]
        ]

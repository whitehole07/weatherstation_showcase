from pathlib import Path

# PATH
GLOBAL_PATH: str = Path(__file__).parent.absolute()

# WEATHER STATION GENERAL CONFIG
WEATHER_STATION_INTERVAL: int = 10

LAT: float = 'lat'
LON: float = 'lon'
ALT: float = 'alt'

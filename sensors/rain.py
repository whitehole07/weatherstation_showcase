from gpiozero import Button
from time import time, sleep
from datetime import datetime

from jsons.json_utilities import JSONUtilities
from utilities.threading_utilities import OneCallThread
from utilities.date_event import TimeEventHandler
from utilities.error_logger import function_log_to_file
from configs.rain_config import RAIN_RATE_RESET_INTERVAL, TIP_MM


class RainGauge(object):
    def __init__(self) -> None:
        """Init Rain Gauge"""
        self.rain_button = Button(6)  # Initialize for button 6

        # Data recovering
        self.rain_dict: dict = JSONUtilities.load_json("rain.json")

        try:
            last_update_json: str = datetime.strptime(self.rain_dict["last_update"], "%Y-%m-%d %H:%M").strftime("%Y-%m-%d")
        except ValueError:
            last_update_json: str = "-"

        self.rainfall_count: int = int(round(self.rain_dict["rain"] / TIP_MM)) if last_update_json == datetime.now().strftime("%Y-%m-%d") else 0.0
        self.rain_rate: float = self.rain_dict["rain_rate"] if last_update_json == datetime.now().strftime("%Y-%m-%d") else 0.0
        self.rain: float = self.count_to_mm(self.rainfall_count)
        self.last_rainfall: float = time()

        # Run Rain-Rate control thread
        if self.rain_rate:
            self.rain_rate_handler()

        # Setting 'when_press' callback
        self.rain_button.when_pressed = self.rainfall_count_callback

        # Decorator Events
        self.__rain_events()

    def _update_rain_dict(self, update_json: bool = True) -> None:
        """It update the current rain dict"""
        self.rain_dict["rain"], self.rain_dict["rain_rate"], self.rain_dict["last_update"] = self.rain, self.rain_rate, datetime.now().strftime("%Y-%m-%d %H:%M")

        if update_json:
            JSONUtilities.save_json("rain.json", self.rain_dict)

    def __rain_events(self) -> None:
        """Time Events"""
        @TimeEventHandler.absolute_time_event('daily')
        def __rainfall_reset() -> None:
            """Daily resets of precipitation"""
            self.rainfall_reset()
            self._update_rain_dict()

    @staticmethod
    def count_to_mm(rainfall_count: int) -> float:
        """Conversion utility"""
        return rainfall_count * TIP_MM  # Count to mm

    @function_log_to_file()
    def rainfall_reset(self) -> None:
        """It resets the rain fell"""
        self.rainfall_count, self.rain = 0, 0.0

    @function_log_to_file()
    def rainfall_count_callback(self) -> None:
        """Main rain callback"""
        self.rainfall_count += 1
        self.last_rainfall = time()  # Update last rainfall time
        self.rain = self.count_to_mm(self.rainfall_count)

        self.rain_rate_handler()  # Starts a thread if one isn't already running

    @OneCallThread.threaded_function(thread_name='rain_rate_thread')
    def rain_rate_handler(self) -> None:
        """Rain Gauge Rain & Rain-Rate Handler"""
        start_time = time()
        prev_rainfall = self.last_rainfall

        if not self.rain_rate:
            self.rain_rate += TIP_MM

        self._update_rain_dict()
        while (time() - start_time) <= RAIN_RATE_RESET_INTERVAL:
            sleep(2.5)
            if self.last_rainfall == prev_rainfall:
                continue

            self.rain_rate = round((TIP_MM / (self.last_rainfall - prev_rainfall)) * 3600, 2)
            prev_rainfall = self.last_rainfall
            self._update_rain_dict()
            start_time = time()

        self.rain_rate = 0.0
        self._update_rain_dict()

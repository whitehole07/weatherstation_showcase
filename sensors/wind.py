import statistics
from gpiozero import Button, MCP3008
from time import time, sleep
from math import pi, radians, sin, cos, atan, degrees
from utilities.threading_utilities import OneCallThread
from utilities.error_logger import function_log_to_file
from others.functions.deg_to_compass import deg_to_compass
from utilities.date_event import remove_by_time_from_list_of_dicts
from configs.wind_config import WIND_INTERVAL, WIND_RESET_INTERVAL, \
    HOW_MANY_MINIMUM_SPINS, ADJUSTMENT, PROBING_DIR_SECS


class WindSpeedGauge(object):
    def __init__(self) -> None:
        self.wind_button = Button(5)  # Initialize for button 5

        # Setting 'when_press' callback
        self.wind_button.when_pressed = self.wind_count_callback

        # Initialize main parameters
        self.wind_gust: float = 0.0
        self.wind: float = 0.0

        self.wind_list: list = []
        self.wind_count: int = 0
        self.last_wind: float = 0.0

        # Thread start
        self.WIND_THREAD_WHILE: bool = True
        self.wind_handler()  # Starts a thread if one isn't already running

    def wind_count_callback(self) -> None:
        self.wind_count += 1
        self.last_wind = time()  # Update last wind time

    @OneCallThread.threaded_function(thread_name='wind_thread')
    @function_log_to_file(filename='wind_handler')
    def wind_handler(self) -> None:
        self.last_wind = time()
        prev_wind: float = self.last_wind

        while self.WIND_THREAD_WHILE:
            self.wind_list = remove_by_time_from_list_of_dicts(self.wind_list, relative_time=WIND_RESET_INTERVAL)

            try:
                assert (time() - prev_wind) <= WIND_INTERVAL
            except AssertionError:
                prev_wind = time()
                self.wind_count = 0
                self.wind_list.append({"wind": 0.0, "time": time(), "weight": WIND_INTERVAL})  # Vento nullo

                self._calc_pure_wind_list()
                self.wind_calc_weighted()
                self.wind_gust_calc()
                continue

            try:
                assert self.wind_count >= HOW_MANY_MINIMUM_SPINS
                wind_m = ((pi * 0.09) * self.wind_count) / (self.last_wind - prev_wind)
                wind = wind_m * 3.6 * ADJUSTMENT
                self.wind_list.append({"wind": wind, "time": time(), "weight": self.last_wind - prev_wind})

                prev_wind = self.last_wind
                self.wind_count = 0

                self._calc_pure_wind_list()
                self.wind_calc_weighted()
                self.wind_gust_calc()
            except AssertionError:
                sleep(0.5)
                continue

    def _calc_pure_wind_list(self):
        self.pure_wind_list = [item["wind"] for item in self.wind_list] if self.wind_list else [0.0]

    def _calc_pure_weight_list(self):
        self.pure_weight_list = [item["weight"] for item in self.wind_list] if self.wind_list else [1.0]

    def wind_gust_calc(self) -> None:
        self.wind_gust = max(self.pure_wind_list)

    def wind_calc_arithmetic(self) -> None:
        self.wind = statistics.mean(self.pure_wind_list)

    def wind_calc_weighted(self) -> None:
        self._calc_pure_weight_list()
        self.wind = sum([wind * weight for wind, weight in zip(self.pure_wind_list, self.pure_weight_list)]) / sum(self.pure_weight_list)


class WindDirectionGauge(object):
    def __init__(self) -> None:
        self.MCP3008 = MCP3008(channel=0)

        # Initialize main parameters
        self.wind_dir_list: list = []
        self.wind_dir_degree: float = 0.0
        self.wind_dir_compass: str = "N"

        # Thread start
        self.WIND_DIR_THREAD_WHILE: bool = True
        self.wind_dir_handler()  # Starts a thread if one isn't already running

    @OneCallThread.threaded_function(thread_name='wind_dir_thread')
    def wind_dir_handler(self) -> None:
        start_time = time()

        while self.WIND_DIR_THREAD_WHILE:
            try:
                assert (time() - start_time) <= WIND_INTERVAL
            except AssertionError:
                self.wind_dir_degree = self.get_average(self.wind_dir_list)
                self.wind_dir_compass = deg_to_compass(self.wind_dir_degree)
                self.wind_dir_list.clear()
                start_time = time()

            direction = self.get_direction()
            if direction != -1:  # If I was able to read the direction
                self.wind_dir_list.append(direction)

            sleep(2.5)

    def get_direction(self) -> float:

        volts = {0.4: 0.0,
                 1.4: 22.5,
                 1.2: 45.0,
                 2.8: 67.5,
                 2.7: 90.0,
                 2.9: 112.5,
                 2.2: 135.0,
                 2.5: 157.5,
                 1.8: 180.0,
                 2.0: 202.5,
                 0.7: 225.0,
                 0.8: 247.5,
                 0.1: 270.0,
                 0.3: 292.5,
                 0.2: 315.0,
                 0.6: 337.5}

        value = round(self.MCP3008.value * 3.3, 1)

        start_time = time()  # Tempo inizio sondaggio dir. vento
        while not (value in volts):  # keep only good measurements
            if time() - start_time > PROBING_DIR_SECS:
                return -1  # It was not able to measure wind direction
            value = round(self.MCP3008.value * 3.3, 1)

        return volts[value]

    @function_log_to_file(filename='wind_dir_get_average')
    def get_average(self, angles: list) -> float:
        if not angles:  # If It was not able to measure wind direction
            return self.wind_dir_degree

        sin_sum = 0.0
        cos_sum = 0.0

        for angle in angles:
            r = radians(angle)
            sin_sum += sin(r)
            cos_sum += cos(r)

        if not cos_sum:
            return angles[-1]

        flen = float(len(angles))
        s = sin_sum / flen
        c = cos_sum / flen
        arc = degrees(atan(s / c))
        average = 0.0

        if s > 0 and c > 0:
            average = arc
        elif c < 0:
            average = arc + 180
        elif s < 0 and c > 0:
            average = arc + 360

        return 360.0 if average == 0.0 else average

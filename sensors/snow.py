from time import sleep
from datetime import datetime

from jsons.json_utilities import JSONUtilities
from others.abstract import Abstract
from utilities.date_event import TimeEventHandler
from utilities.error_logger import function_log_to_file
from utilities.threading_utilities import OneCallThread


class SnowGauge(object):  # Not a real gauge so far
    def __init__(self) -> None:
        """Init Snow Gauge"""

        # Data recovering
        self.snow_dict: dict = JSONUtilities.load_json("snow.json")

        try:
            last_update_json: str = datetime.strptime(self.snow_dict["last_update"], "%Y-%m-%d %H:%M").strftime("%Y-%m-%d")
            last_update_json_hour: str = datetime.strptime(self.snow_dict["last_update"], "%Y-%m-%d %H:%M").strftime("%Y-%m-%d %H")
        except ValueError:
            last_update_json, last_update_json_hour = "-", "-"

        self.snow: float = self.snow_dict["snow"] if last_update_json == datetime.now().strftime("%Y-%m-%d") else 0.0
        self.snow_rate: float = self.snow_dict["snow_rate"] if last_update_json == datetime.now().strftime("%Y-%m-%d") else 0.0

        # Initial update
        if last_update_json_hour != datetime.now().strftime("%Y-%m-%d %H"):
            self.update_snow()

        # Decorator Events
        self.__snow_events()

    def _update_snow_dict(self, update_json: bool = True) -> None:
        """It update the current snow dict"""
        self.snow_dict["snow"], self.snow_dict["snow_rate"], self.snow_dict["last_update"] = self.snow, self.snow_rate, datetime.now().strftime("%Y-%m-%d %H:%M")

        if update_json:
            JSONUtilities.save_json("snow.json", self.snow_dict)

    def __snow_events(self) -> None:
        """Time Events"""
        @TimeEventHandler.absolute_time_event('daily')
        def __snowfall_reset() -> None:
            """Daily resets of precipitation"""
            self.snowfall_reset()
            self._update_snow_dict()

        @TimeEventHandler.absolute_time_event('hourly')
        def __update_snow() -> None:
            """Snow update"""
            self.update_snow()

    @function_log_to_file()
    def snowfall_reset(self) -> None:
        """It resets the snow fell"""
        self.snow = 0.0

    @OneCallThread.threaded_function(thread_name='snow_rate_thread')
    def update_snow(self) -> None:
        """It gets the current snow and snow rate"""
        while (datetime.fromtimestamp(Abstract.current_forecast_provider.get_current_time())).hour != datetime.now().hour:
            sleep(2.5)
            continue

        self.snow_rate = Abstract.current_forecast_provider.get_snow()
        self.snow += self.snow_rate

        self._update_snow_dict()

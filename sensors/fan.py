from gpiozero import PWMOutputDevice
from configs.fan_config import FAN_PIN, FREQUENCY, DUTY_CYCLE_BLYNK_PIN

from config import GLOBAL_PATH
from utilities.decorator_error_handler import retry


class ActiveFan(object):
    def __init__(self) -> None:
        """Fan Init"""
        # Superclass init
        self.fan = PWMOutputDevice(FAN_PIN, frequency=FREQUENCY)

        # Fan initialization
        self.init_fan()

    def init_fan(self) -> None:
        """Init procedure of the fan"""
        self.fan.blink(on_time=0.5, off_time=1.5, n=5, background=False)
        self.fan.on()

    @retry(TypeError, tries=1, log_path=GLOBAL_PATH)
    def fan_set(self, obj) -> None:
        """It sets the correct state of the fan"""
        if (obj.solar_radiation >= 150.0 and obj.wind <= 2.5) or \
                (obj.solar_radiation < 150.0 and obj.wind < 0.5) or \
                obj.humidity_mean >= 98.0 or \
                obj.rain_rate or \
                obj.snow_rate:
            duty_cycle = 100
        else:
            duty_cycle = 0

        self.fan.value = duty_cycle / 100
        obj.blynk.virtual_write(DUTY_CYCLE_BLYNK_PIN, duty_cycle)

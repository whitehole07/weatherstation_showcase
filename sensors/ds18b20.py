from w1thermsensor import W1ThermSensor
from time import time, sleep
from statistics import mean

from utilities.threading_utilities import OneCallThread
from utilities.decorator_error_handler import retry
from configs.ds18b20_config import MEAN_THP_INTERVAL


class DS18B20(object):
    def __init__(self):
        # Instance init
        self._initialize_sensor()

        # Attributes Init
        self._ds18b20_update_values([self.ds18b20.get_temperature()])

        # Thread start
        self.DS18B20_THREAD_WHILE: bool = True
        self.ds18b20_handler()  # Starts a thread if one isn't already running

    def _initialize_sensor(self):
        """Initialises the sensor"""
        if hasattr(self, "ds18b20"):
            del self.ds18b20

        self.ds18b20 = W1ThermSensor()

    @OneCallThread.threaded_function(thread_name='ds18b20_thread')
    @retry(IndexError, self_callback="_initialize_sensor")
    def ds18b20_handler(self):
        while self.DS18B20_THREAD_WHILE:
            temp_list: list = []  # 0 - temp
            start_time = time()
            while (time() - start_time) <= MEAN_THP_INTERVAL:
                temp_list.append(self.ds18b20.get_temperature())
                sleep(1)
            self._ds18b20_update_values(temp_list)
            sleep(2.5)

    def _ds18b20_update_values(self, temperature_list: list):
        self.DS18B20_temperature_mean = mean(temperature_list)

from statistics import mean
from time import sleep, time

from board import SCL, SDA
from busio import I2C
from adafruit_bme280 import Adafruit_BME280_I2C

from config import GLOBAL_PATH
from utilities.decorator_error_handler import retry
from configs.bme280_config import MEAN_THP_INTERVAL
from utilities.threading_utilities import OneCallThread


class BME280(object):
    def __init__(self) -> None:
        # Instance init
        self.bme280 = Adafruit_BME280_I2C(I2C(SCL, SDA), address=0x76)  # temperature, pressure, humidity are in here

        # Attributes Init
        self._bme280_update_values([self.bme280.temperature], [self.bme280.humidity], [self.bme280.pressure])

        # Thread start
        self.BME280_THREAD_WHILE: bool = True
        self.bme280_handler()  # Starts a thread if one isn't already running

    def _reset_sensor(self):
        """Initialises the sensor"""
        if hasattr(self, "bme280"):
            del self.bme280

        self.bme280 = Adafruit_BME280_I2C(I2C(SCL, SDA), address=0x76)  # temperature, pressure, humidity are in here

    @OneCallThread.threaded_function(thread_name='bme280_thread')
    @retry(OSError, self_callback="_reset_sensor", log_path=GLOBAL_PATH)
    def bme280_handler(self) -> None:
        while self.BME280_THREAD_WHILE:
            params_matrix: list = [[], [], []]  # 0 - temp, 1 - humy, 2 - press
            start_time = time()
            while (time() - start_time) <= MEAN_THP_INTERVAL:
                for i, param in enumerate([self.bme280.temperature, self.bme280.humidity, self.bme280.pressure]):
                    params_matrix[i].append(param)
                sleep(1)
            self._bme280_update_values(params_matrix[0], params_matrix[1], params_matrix[2])
            sleep(2.5)

    def _bme280_update_values(self, temperature_list: list, humidity_list: list, pressure_list: list) -> None:
        self.BME280_temperature_mean = mean(temperature_list)
        self.humidity_mean = mean(humidity_list)
        self.pressure_mean = mean(pressure_list)

from re import sub


def get_main_weather(conv_dict: dict, code: str or int) -> str:
    if str(code)[0] in ["2", "3", "5", "6"]:
        second: str = "x"
        third: str = "x"
    else:
        second: str = sub("[123456789]", "x", str(code)[1])
        third: str = "x" if second == "x" else sub("[123456789]", "x", str(code)[2])

    return conv_dict[str(code)[0] + second + third]

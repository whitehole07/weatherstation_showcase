__all__ = ["deg_to_compass"]


def deg_to_compass(angle: float) -> str:
    val = int((angle / 22.5) + .5)

    arr = ["N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW"]

    return arr[(val % 16)]

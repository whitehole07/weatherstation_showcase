__all__ = ["cloud_base"]


def cloud_base(temperature: float, dew_point: float) -> int:
    h: float = 125.0 * (temperature - dew_point)
    if h < 0:
        h = 0
    return int(h)

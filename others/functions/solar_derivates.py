from datetime import datetime


def get_solar_percentage(sunrise: datetime, sunset: datetime) -> float:
    if (now := datetime.now(sunrise.tzinfo)) <= (sunrise := sunrise):
        return 0.0
    elif now >= (sunset := sunset):
        return 0.0
    else:
        return 100 - ((now.timestamp() - sunrise.timestamp()) / (sunset.timestamp() - sunrise.timestamp()) * 100)


def get_solar_percentage_variation_string(precision: int, sunrise: datetime, sunset: datetime) -> str:
    if not sunrise or not sunset:
        return f"▰  +{'%.{}f'.format(precision) % 0.0}%/h"

    if (now := datetime.now(sunrise.tzinfo)) <= sunrise:
        return f"▰  +{'%.{}f'.format(precision) % 0.0}%/h"
    elif now >= (sunset := sunset):
        return f"▰  +{'%.{}f'.format(precision) % 0.0}%/h"
    else:
        return f"▼  -{'%.{}f'.format(precision) % (3600 / (sunset.timestamp() - sunrise.timestamp()) * 100)}%/h"

__all__ = ["feels_like"]

from math import fabs, sqrt
from utilities.unit_conversion import UnitConversion


def feels_like(vTemperature, vRelativeHumidity, vWindSpeed) -> float:
    vTemperature = UnitConversion.celsius_to_farenheit(vTemperature)
    # Try Wind Chill first
    if vTemperature <= 50 and vWindSpeed >= 3:
        vFeelsLike = 35.74 + (0.6215 * vTemperature) - 35.75 * (vWindSpeed ** 0.16) + (
                (0.4275 * vTemperature) * (vWindSpeed ** 0.16))
    else:
        vFeelsLike = vTemperature

    # Replace it with the Heat Index, if necessary
    if vFeelsLike == vTemperature and vTemperature >= 80:
        vFeelsLike = 0.5 * (vTemperature + 61.0 + ((vTemperature - 68.0) * 1.2) + (vRelativeHumidity * 0.094))

        if vFeelsLike >= 80:
            vFeelsLike = -42.379 + 2.04901523 * vTemperature + 10.14333127 * vRelativeHumidity - .22475541 * vTemperature * vRelativeHumidity - .00683783 * vTemperature * vTemperature - .05481717 * vRelativeHumidity * vRelativeHumidity + .00122874 * vTemperature * vTemperature * vRelativeHumidity + .00085282 * vTemperature * vRelativeHumidity * vRelativeHumidity - .00000199 * vTemperature * vTemperature * vRelativeHumidity * vRelativeHumidity
            if vRelativeHumidity < 13 and 80 <= vTemperature <= 112:
                vFeelsLike = vFeelsLike - ((13 - vRelativeHumidity) / 4) * sqrt(
                    (17 - fabs(vTemperature - 95.)) / 17)
                if vRelativeHumidity > 85 and 80 <= vTemperature <= 87:
                    vFeelsLike = vFeelsLike + ((vRelativeHumidity - 85) / 10) * ((87 - vTemperature) / 5)

    return UnitConversion.farenheit_to_celsius(vFeelsLike)

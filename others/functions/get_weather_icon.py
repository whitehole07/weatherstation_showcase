from jsons.json_utilities import JSONUtilities

code_to_icon_dict: dict = JSONUtilities.load_json("code_to_icon.json")


def get_weather_icon(cwc: bool, clouds: float, code: str or int, day_n_night: str) -> str:
    # CUSTOM ICON SECTION
    if cwc:
        if clouds <= 50:
            if str(code).startswith("6"):
                return code_to_icon_dict[f'623{day_n_night}']
            if str(code).startswith("5"):
                return code_to_icon_dict[f'531{day_n_night}']

    return code_to_icon_dict[f'{code}{day_n_night}']

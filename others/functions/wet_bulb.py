__all__ = ["wet_bulb"]

from math import exp, log


def sat_press(tdb) -> float:
    """ Function to compute saturation vapor pressure in [kPa]
        ASHRAE Fundamentals handbood (2005) p 6.2, equation 5 and 6
            Tdb = Dry bulb temperature [degC]
            Valid from -100C to 200 C
    """

    C1 = -5674.5359
    C2 = 6.3925247
    C3 = -0.009677843
    C4 = 0.00000062215701
    C5 = 2.0747825E-09
    C6 = -9.484024E-13
    C7 = 4.1635019
    C8 = -5800.2206
    C9 = 1.3914993
    C10 = -0.048640239
    C11 = 0.000041764768
    C12 = -0.000000014452093
    C13 = 6.5459673

    TK = tdb + 273.15  # Converts from degC to degK

    if TK <= 273.15:
        result = exp(C1 / TK + C2 + C3 * TK + C4 * TK ** 2 + C5 * TK ** 3 +
                     C6 * TK ** 4 + C7 * log(TK)) / 1000
    else:
        result = exp(C8 / TK + C9 + C10 * TK + C11 * TK ** 2 + C12 * TK ** 3 +
                     C13 * log(TK)) / 1000
    return result


def hum_rat(Tdb, Twb, P) -> float:
    """ Function to calculate humidity ratio [kg H2O/kg air]
        Given dry bulb and wet bulb temp inputs [degC]
        ASHRAE Fundamentals handbood (2005)
            Tdb = Dry bulb temperature [degC]
            Twb = Wet bulb temperature [degC]
            P = Ambient Pressure [kPa]
    """

    Pws = sat_press(Twb)
    Ws = 0.62198 * Pws / (P - Pws)  # Equation 23, p6.8
    if Tdb >= 0:  # Equation 35, p6.9
        result = (((2501 - 2.326 * Twb) * Ws - 1.006 * (Tdb - Twb)) /
                  (2501 + 1.86 * Tdb - 4.186 * Twb))
    else:  # Equation 37, p6.9
        result = (((2830 - 0.24 * Twb) * Ws - 1.006 * (Tdb - Twb)) /
                  (2830 + 1.86 * Tdb - 2.1 * Twb))
    return result


def hum_rat2(Tdb, RH, P) -> float:
    """ Function to calculate humidity ratio [kg H2O/kg air]
        Given dry bulb and wet bulb temperature inputs [degC]
        ASHRAE Fundamentals handbood (2005)
            Tdb = Dry bulb temperature [degC]
            RH = Relative Humidity [Fraction or %]
            P = Ambient Pressure [kPa]
    """
    Pws = sat_press(Tdb)
    result = 0.62198 * RH * Pws / (P - RH * Pws)  # Equation 22, 24, p6.8
    return result


def _wet_bulb(Tdb, RH, P) -> float:
    """ Calculates the Wet Bulb temp given:
            Tdb = Dry bulb temperature [degC]
            RH = Relative humidity ratio [Fraction or %]
            P = Ambient Pressure [kPa]
        Uses Newton-Rhapson iteration to converge quickly
    """

    W_normal = hum_rat2(Tdb, RH, P)
    result = Tdb

    'Solves to within 0.001% accuracy using Newton-Rhapson'
    W_new = hum_rat(Tdb, result, P)
    while abs((W_new - W_normal) / W_normal) > 0.00001:
        W_new2 = hum_rat(Tdb, result - 0.001, P)
        dw_dtwb = (W_new - W_new2) / 0.001
        result = result - (W_new - W_normal) / dw_dtwb
        W_new = hum_rat(Tdb, result, P)
    return result


def wet_bulb(Tdb, RH, P) -> float:
    """ Calculates the Wet Bulb temp given:
                Tdb = Dry bulb temperature [degC]
                RH = Relative humidity ratio [Fraction or %]
                P = Ambient Pressure [hPa]
            Uses Newton-Rhapson iteration to converge quickly
        """
    return _wet_bulb(Tdb, RH / 100.0, P / 10.0)

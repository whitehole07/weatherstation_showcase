from config import ALT
from math import exp


def slp_to_zlp(P0: float, Tz: float):
    """Computes the pressure at given altitude"""
    g: float = 9.80665
    M: float = 0.0289644
    R: float = 8.31432
    h: float = ALT

    return P0 * exp((-g*M*h)/(R*Tz))

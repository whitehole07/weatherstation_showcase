from meteocalc import dew_point

from meteogram.main import MeteoGram
from others.functions.cloud_base import cloud_base
from others.functions.feels_like import feels_like
from others.functions.wet_bulb import wet_bulb
from others.functions.solar_derivates import get_solar_percentage
from weather_underground.main import WeatherUnderground
from others.abstract import Abstract


class DerivedParameters(object):
    def __init__(self, obj) -> None:

        # WeatherStation Instance
        self.obj = obj

        # Init derived parameters
        self._get_derived_parameters()

    def update_derived_parameters(self) -> None:
        self._get_derived_parameters()

    def _get_derived_parameters(self) -> None:
        self.dew_point = self.get_dewpoint()
        self.cloud_base = self.get_cloud_base()
        self.wet_bulb = self.get_wetbulb()

        if hasattr(self.obj, 'wind'):
            self.feels_like = self.get_feelslike(float(self.obj.wind))

        # WeatherUnderground
        self.uv_index = WeatherUnderground.get_uvindex()
        self.solar_radiation = WeatherUnderground.get_solar()

        # Multi - Provider
        # Current Condition
        self.cc_weather, self.cc_desc, self.cc_icon_url = Abstract.current_forecast_provider.get_current_conditions()
        self.visibility = Abstract.current_forecast_provider.get_visibility()
        self.cloudiness = Abstract.current_forecast_provider.get_cloudiness()
        self.exp_prep_msg, self.exp_prep, self.exp_prep_flag = Abstract.current_forecast_provider.get_precipitation(Abstract.hourly_forecast_provider.__name__)
        self.sunrise, self.sunset = Abstract.current_forecast_provider.get_sunrise().strftime("%H : %M : %S"), Abstract.current_forecast_provider.get_sunset().strftime("%H : %M : %S")
        self.solar_percentage = get_solar_percentage(Abstract.current_forecast_provider.get_sunrise(), Abstract.current_forecast_provider.get_sunset())

        # MeteoGram
        self.meteogram_url = MeteoGram.meteogram_request()
        self.next_meteogram_update = MeteoGram.get_next_update()

    def get_dewpoint(self) -> float:
        return dew_point(self.obj.temperature_mean, self.obj.humidity_mean).value

    def get_wetbulb(self) -> float:
        return wet_bulb(self.obj.temperature_mean, self.obj.humidity_mean, self.obj.pressure_mean)

    def get_cloud_base(self) -> int:
        return cloud_base(self.obj.temperature_mean, self.get_dewpoint())

    def get_feelslike(self, wind: float) -> float:
        return feels_like(self.obj.temperature_mean, self.obj.humidity_mean, wind)

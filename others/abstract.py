__all__ = ["Abstract"]

from jsons.json_utilities import JSONUtilities
from configs.blynk_config import API_MAP

settings_dict: dict = JSONUtilities.load_json("settings.json")


class Abstract(object):

    # API Providers
    current_forecast_provider = API_MAP[settings_dict["current_forecast"]]
    hourly_forecast_provider = API_MAP[settings_dict["hourly_forecast"]]
    daily_forecast_provider = API_MAP[settings_dict["daily_forecast"]]

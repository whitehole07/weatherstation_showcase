__all__ = ["UnitConversion"]


class UnitConversion:
    @staticmethod
    def hpa_to_inches(press: float) -> float:
        return press * 0.02953

    @staticmethod
    def kph_to_mph(wind: float) -> float:
        return wind * 0.621371

    @staticmethod
    def celsius_to_farenheit(temp: float) -> float:
        return (temp * 9/5) + 32

    @staticmethod
    def farenheit_to_celsius(temp: float) -> float:
        return (5.0/9)*(temp - 32)

    @staticmethod
    def mm_to_inches(rainfall: int) -> float:
        return rainfall * 0.0393701

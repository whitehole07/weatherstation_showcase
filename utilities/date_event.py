__all__ = ['TimeEventHandler', 'remove_by_time_from_list_of_dicts']

from datetime import datetime
from time import sleep
from utilities.threading_utilities import threaded_function


def remove_by_time_from_list_of_dicts(list_of_dicts: list, *, relative_time: float, key: str = "time") -> list:
    """ Removes all element with relative time > relative time argument passed
    Note: Times must be timestamps """
    return [item for item in list_of_dicts if (datetime.now().timestamp() - item[key]) <= relative_time]


class __DateEventsHandler(object):  # Date Events handler

    def __init__(self):
        # Register Section
        self._absolute_handlers: dict = {
            "daily": [],
            "hourly": []}
        self._relative_handlers: list = []

        # Data section
        self.date = datetime.now()
        self.DATE_WHILE: bool = True

        # Start
        self.start()

    def start(self):  # Starts date_check_threads
        self.__date_check_thread()

    def _stop(self):  # Stops date_check_threads
        self.DATE_WHILE = False

    @threaded_function
    def __date_check_thread(self):  # Checks if there are date updates
        prev_day = self.date.day
        prev_hour = self.date.hour
        while self.DATE_WHILE:
            if self.date.day != prev_day:  # If day changed
                for func in self._absolute_handlers["daily"]:
                    func()
                prev_day = datetime.now().day

            if self.date.hour != prev_hour:  # If hour changed
                for func in self._absolute_handlers["hourly"]:
                    func()
                prev_hour = datetime.now().hour

            for i, (func, waiting, last_datetime) in enumerate(self._relative_handlers):
                if (self.date - last_datetime).total_seconds() >= waiting:
                    func()
                    self._relative_handlers[i] = (func, waiting, datetime.now())

            self.date = datetime.now()
            sleep(1)

    def absolute_time_event(self, when: str):
        def wrapper(func):
            if when in self._absolute_handlers and func not in self._absolute_handlers[when]:
                self._absolute_handlers[when].append(func)
            return func
        return wrapper

    def relative_time_event(self, time: int):
        def wrapper(func):
            if (func, time, datetime.now()) not in self._relative_handlers:
                self._relative_handlers.append((func, time, datetime.now()))
            return func
        return wrapper


TimeEventHandler = __DateEventsHandler()

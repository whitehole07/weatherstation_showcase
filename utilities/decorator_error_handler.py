__all__ = ["retry"]

from time import sleep
from os.path import exists
from os import mkdir
from datetime import datetime
from traceback import format_exc


class Infinite(object):
    """Infinite for iterator"""

    def __iter__(self):
        return self

    def __next__(self) -> None:
        return None


def retry(exception: tuple or BaseException, log_path: str = '', callback=None, self_callback: str = None, tries: int = None, delay: float = 0, jitter: float = 0):
    def wrapper(func):
        def inner_wrapper(*args, **kwargs):
            wait = delay

            if type(tries) is int:
                rn: range = range(tries)
            else:
                rn: Infinite = Infinite()

            # noinspection PyBroadException
            for _ in rn:
                try:
                    return func(*args, **kwargs)
                except exception:
                    if log_path:
                        if not exists(f"{log_path}/retry_logs"):  # It creates a dir if doesn't exist
                            mkdir(f"{log_path}/retry_logs")

                        with open(f"{log_path}/retry_logs/{func.__name__}.txt", "a") as f:  # It writes the error
                            f.write(f"{datetime.now().strftime('%d/%m/%Y - %H:%M:%S')}: \n{format_exc()}\n")

                    if callable(callback):
                        callback()  # No arguments here

                    if self_callback and args:
                        try:
                            getattr(args[0], self_callback)()
                        except AttributeError:
                            pass
                    sleep(wait)
                    wait += jitter
        return inner_wrapper
    return wrapper

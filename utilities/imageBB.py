__all__ = ["ImageBB"]

from base64 import b64encode
from requests import post as POST

from utilities.decorator_error_handler import retry
from config import GLOBAL_PATH


class RequestError(BaseException):
    pass


class ImageBB(object):
    key: str = "key"

    @classmethod
    @retry(BaseException, tries=3, delay=10, jitter=5, log_path=GLOBAL_PATH)
    def upload_image(cls, image: bytes, expiration: int = 0) -> str:
        url = "https://api.imgbb.com/1/upload"
        payload = {
            "key": cls.key,
            "image": b64encode(image),
            "expiration": expiration,
        }
        r = POST(url, payload, timeout=60)
        data_dict: dict = r.json()

        if not (data_dict.get("data")):
            raise RequestError(f"Response object: {r},\n Data dict: {data_dict}")

        return data_dict["data"].get("url") if data_dict else ""

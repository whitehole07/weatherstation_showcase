__all__ = ["Operators"]


class Operators:
    @staticmethod
    def lower(input1: float, input2: float):
        return input1 < input2

    @staticmethod
    def greater(input1: float, input2: float):
        return input1 > input2

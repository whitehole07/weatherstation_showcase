__all__ = ['threaded_function', 'OneCallThread', 'LoopHandler']

from threading import Thread
from time import sleep


def threaded_function(func):  # Simple thread decorator
    def wrapper(*args, **kwargs):
        tr = Thread(target=func, args=args, kwargs=kwargs)
        tr.setDaemon(True)
        tr.start()
    return wrapper


class __LoopHandler(object):

    def __init__(self):
        self.loop = None
        self.wait: int = 0

    def main_loop(self, wait_between_cycles: int):
        def wrapper(func):
            if not self.loop:
                self.loop = func
                self.wait = wait_between_cycles
            return func
        return wrapper

    def executor_loop(self):
        while True:
            try:
                self.loop()
                sleep(self.wait)
            except KeyboardInterrupt:
                break


class OneCallThread(object):  # Threads that can be called once
    threads_dict: dict = {}

    @classmethod
    def threaded_function(cls, thread_name: str):
        def wrapper(func):
            def inner_wrapper(*args, **kwargs):
                if thread_name in cls.threads_dict.keys() and cls.threads_dict[thread_name].is_alive():
                    return

                tr = Thread(target=func, args=args, kwargs=kwargs)
                tr.setDaemon(True)
                tr.start()
                cls.threads_dict[thread_name] = tr
            return inner_wrapper
        return wrapper


LoopHandler = __LoopHandler()

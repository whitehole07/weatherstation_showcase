__all__ = ["function_log_to_file", "log_to_file"]

from datetime import datetime
from traceback import format_exc
from config import GLOBAL_PATH
from sys import exit
from os.path import exists
from os import mkdir


def log_to_file(filename: str, error: str, end: bool = True):
    if not exists(f"{GLOBAL_PATH}/logs"):  # It creates a dir if doesn't exist
        mkdir(f"{GLOBAL_PATH}/logs")

    with open(f"{GLOBAL_PATH}/logs/{filename}.txt", "a") as f:  # It writes the error
        f.write(f"{datetime.now().strftime('%d/%m/%Y - %H:%M:%S')}: \n{error}\n")

    if end:
        exit(-1)


def function_log_to_file(filename: str = None):
    def inner_wrapper(func):
        def wrapper(*args, **kwargs):
            # noinspection PyBroadException
            try:
                return func(*args, **kwargs)
            except BaseException:
                log_to_file(filename if filename is not None else func.__name__, format_exc())
        return wrapper
    return inner_wrapper

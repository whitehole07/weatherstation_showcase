# CLIMACELL
API_KEY: str = 'API_KEY'
PER_DAY_API_DAILY_REQUESTS: int = 48
PER_DAY_API_HOURLY_REQUESTS: int = 48
PER_DAY_API_NOWCAST_REQUESTS: tuple = (288, 576)
PER_DAY_API_CURRENT_REQUESTS: int = 400
HOUR_COUNT_PRP: bool = True
CUSTOM_WEATHER_ICON_CONDITIONS: bool = True


# PORT
PORT: dict = {
    "rain_heavy": "504",
    "rain": "502",
    "rain_light": "501",
    "freezing_rain_heavy": "511",
    "freezing_rain": "511",
    "freezing_rain_light": "511",
    "freezing_drizzle": "302",
    "drizzle": "301",
    "ice_pellets_heavy": "613",
    "ice_pellets": "611",
    "ice_pellets_light": "612",
    "snow_heavy": "602",
    "snow": "601",
    "snow_light": "600",
    "flurries": "620",
    "tstorm": "202",
    "fog_light": "701",
    "fog": "711",
    "cloudy": "804",
    "mostly_cloudy": "803",
    "partly_cloudy": "802",
    "mostly_clear": "801",
    "clear": "800",
}

DESC_PORT: dict = {
    "rain_heavy": ("Rain", "Heavy intensity rain"),
    "rain": ("Rain", "Moderate rain"),
    "rain_light": ("Rain", "Light rain"),
    "freezing_rain_heavy": ("Rain", "Heavy freezing rain"),
    "freezing_rain": ("Rain", "Freezing rain"),
    "freezing_rain_light": ("Rain", "Light freezing rain"),
    "freezing_drizzle": ("Drizzle", "Light freezing drizzle"),
    "drizzle": ("Drizzle", "Moderate drizzle"),
    "ice_pellets_heavy": ("Snow", "Heavy ice pellets"),
    "ice_pellets": ("Snow", "Moderate Ice pellets"),
    "ice_pellets_light": ("Snow", "Light ice pellets"),
    "snow_heavy": ("Snow", "Heavy snow"),
    "snow": ("Snow", "Moderate snow"),
    "snow_light": ("Snow", "Light snow"),
    "flurries": ("Flurries", "Light snow showers"),
    "tstorm": ("Thunderstorm", "Thunderstorm conditions"),
    "fog_light": ("Fog", "Light fog"),
    "fog": ("Fog", "Fog"),
    "cloudy": ("Clouds", "Overcast clouds"),
    "mostly_cloudy": ("Clouds", "Broken clouds"),
    "partly_cloudy": ("Clouds", "Scattered clouds"),
    "mostly_clear": ("Clouds", "Few clouds"),
    "clear": ("Clear", "Clear sky"),
}

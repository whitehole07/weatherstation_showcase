# WEATHERBIT
API_KEY: str = 'API_KEY'
PER_DAY_API_DAILY_REQUESTS: int = 48
PER_DAY_API_CURRENT_REQUESTS: int = 400
CUSTOM_WEATHER_ICON_CONDITIONS: bool = True


# PORT
PORT: dict = {
    233: "202",
    500: "501",
    501: "502",
    502: "503",
    610: "613",
    612: "613",
    623: "620",
    700: "701",
    900: "711"
}

MAIN_DICT: dict = {
    "2xx": "Thunderstorm",
    "3xx": "Drizzle",
    "5xx": "Rain",
    "6xx": "Snow",
    "701": "Mist",
    "711": "Smoke",
    "721": "Haze",
    "731": "Dust",
    "741": "Fog",
    "751": "Fog",
    "800": "Clear",
    "80x": "Clouds",
    "900": "Unknown",
}

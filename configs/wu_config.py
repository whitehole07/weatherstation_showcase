# WEATHER UNDERGROUND
WU_STATION_ID: str = 'STATION_ID'
WU_STATION_PWD: str = 'STATION_PWD'
WU_APIKEY: str = 'API_KEY'
WU_ADIACENT_STATION_ID: str = 'ADIACENT_ID'  # Alternativa: 'ALTERNATIVA'
PER_DAY_API_CURRENT_REQUESTS: int = 288  # + 24 Historical (Limite di 1500 richieste al giorno e di 30 al minuto)

from providers.openweather.main import OpenWeather
from providers.weatherbit.main import WeatherBit
from providers.climacell.main import ClimaCell

# BLYNK
BLYNK_AUTH_TOKEN: str = 'BLYNK_TOKEN'
VM_HOST: str = 'VM_IP'

# GLOBAL V-PINS MAP
BLYNK_GLOBAL_MAP = {
    "temperature_mean": [0],
    "humidity_mean": [1],
    "pressure_mean": [2, 11],
    "dew_point": [3],
    "wind": [4],
    "wind_gust": [5],
    "rain": [6],
    "snow": [26],
    "snow_rate": [184],
    "rain_rate": [7],
    "wind_dir_compass": [8],
    "solar_radiation": [9],
    "uv_index": [10],
    "last_update": [12],
    "wind_dir_degree": [15],
    "cloud_base": [16],
    "feels_like": [18],
    "wet_bulb": [19],
    "cpu_temp": [20],
    "rel_uptime": [29],
    "temp_0": [200],
    "temp_1": [201],
    "cc_weather": [21],
    "cc_desc": [27],
    "cc_icon_url": [22],
    "meteogram_url": [190],
    "next_meteogram_update": [179],
    "tempHigh": [51],
    "tempLow": [52],
    "pressureMax": [55],
    "pressureMin": [54],
    "humidityHigh": [60],
    "humidityLow": [59],
    "precipTotal": [57],
    "winddirAvg": [185],
    "precipRate": [186],
    "dewptHigh": [58],
    "dewptLow": [56],
    "windgustHigh": [53],
    "solarRadiationHigh": [62],
    "uvHigh": [63],
    "historical_log": [61],
    "visibility": [23],
    "cloudiness": [24],
    "exp_prep_msg": [36],
    "exp_prep": [37],
    "sunrise": [175],
    "sunset": [176],
    "solar_percentage": [177],
    **{f"day{i}_pop": [x] for i, x in enumerate([28, 33, 34, 35, 92, 93, 133, 134])},
    **{f"day{i}_temp_max": [x] for i, x in enumerate([38, 39, 40, 41, 94, 95, 135, 136])},
    **{f"day{i}_temp_min": [x] for i, x in enumerate([42, 43, 44, 45, 96, 97, 137, 138])},
    **{f"day{i}_weather": [x] for i, x in enumerate([46, 47, 48, 49, 98, 99, 139, 140])},
    **{f"day{i}_weather_icon_url": [x] for i, x in enumerate([64, 65, 66, 67, 119, 120, 141, 142])},
    **{f"day{i}_rain": [x] for i, x in enumerate([68, 69, 70, 71, 121, 122, 143, 144])},
    **{f"day{i}_snow": [x] for i, x in enumerate([88, 89, 90, 91, 123, 124, 145, 146])},
    **{f"day{i}_when": [x] for i, x in enumerate([72, 73, 74, 75, 125, 126, 147, 148])},
    **{f"day{i}_clouds": [x] for i, x in enumerate([76, 77, 78, 79, 127, 128, 149, 150])},
    **{f"day{i}_wind_speed": [x] for i, x in enumerate([80, 81, 82, 83, 129, 130, 151, 152])},
    **{f"day{i}_wind_dir": [x] for i, x in enumerate([84, 85, 86, 87, 131, 132, 153, 154])},
    "hour_when": [156],
    "hour_temp": [157],
    "hour_dew_point": [158],
    "hour_humidity": [159],
    "hour_visibility": [160],
    "hour_pressure": [161],
    "hour_weather": [162],
    "hour_description": [163],
    "hour_weather_icon_url": [164],
    "hour_pop": [165],
    "hour_rain": [166],
    "hour_snow": [167],
    "hour_wind_speed": [168],
    "hour_wet_bulb": [169],
    "hour_wind_dir": [170],
    "hour_clouds": [171],
}

# TEMPERATURE SENSORS INDEX MAP
TEMP_SENS_INDEX_MAP: dict = {
    '1': "BME280",
    '2': "DS18B20"
}

# API PROVIDERS INDEX MAP
API_MAP: dict = {
    "OpenWeather": OpenWeather,
    "WeatherBit": WeatherBit,
    "ClimaCell": ClimaCell
}

API_PROVIDERS_INDEX_MAP_CURRENT: dict = {
    '1': "OpenWeather",
    '2': "WeatherBit",
    '3': "ClimaCell"
}
API_PROVIDERS_INDEX_MAP_HOURLY: dict = {
    '1': "OpenWeather",
    '2': "ClimaCell"
}
API_PROVIDERS_INDEX_MAP_DAILY: dict = {
    '1': "OpenWeather",
    '2': "WeatherBit",
    '3': "ClimaCell"
}

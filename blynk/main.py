from datetime import datetime
from importlib import reload

from blynklib import Blynk
from bs4 import BeautifulSoup
from requests import get as GET
from requests.exceptions import ConnectionError as RequestConnectionError
from requests.exceptions import ReadTimeout

import blynk.settings.label_settings
import blynk.settings.color_settings
from blynk.gradient import get_gradient
from blynk.settings.forecast_settings import FORECAST_INTERVAL, LONG_RANGE_VPINS_MAP
from blynk.settings.radar_webcam_settings import WEBCAM_ARGS, WEBCAMLONG_ARGS, \
    RADAR_ARGS, ALERTS_ARGS, MAP_ARGS, WRAM_VPINS_MAP, WEBCAM_INTERVAL, OTHER_RADAR_ARGS
from blynk.settings.records_settings import RECORDS_VPINS_MAP
from blynk.settings.urls_settings import URLS_PARAMS, URLS_HOURLY_FORECAST
from blynk.settings.value_settings import BLYNK_VPINS_FORMAT_MAP, HOURLY_FORECAST_FORMAT, HISTORICAL_FORMAT
from config import GLOBAL_PATH
from configs.blynk_config import BLYNK_AUTH_TOKEN, VM_HOST, BLYNK_GLOBAL_MAP, TEMP_SENS_INDEX_MAP, \
    API_PROVIDERS_INDEX_MAP_CURRENT, API_PROVIDERS_INDEX_MAP_HOURLY, API_PROVIDERS_INDEX_MAP_DAILY, API_MAP
from configs.mgram_config import METEOGRAM_BLYNK_TABLE_PIN, METEOGRAM_BLYNK_COMMAND_PIN
from jsons.json_utilities import JSONUtilities
from meteogram.main import MeteoGram
from others.functions.deg_to_compass import deg_to_compass
from utilities.date_event import TimeEventHandler
from utilities.decorator_error_handler import retry
from utilities.error_logger import function_log_to_file
from utilities.threading_utilities import OneCallThread
from weather_underground.main import WeatherUnderground
from others.abstract import Abstract


class BlynkMain(object):
    def __init__(self) -> None:
        # It tries to establish a connection with blynk
        self.blynk_connect()

        # Main thread start
        self.BLYNK_THREAD_WHILE: bool = True
        self.main_run()

    def __connect(self) -> None:
        """Connects to Blynk Server"""
        if hasattr(self, "blynk"):
            del self.blynk

        self.blynk = Blynk(BLYNK_AUTH_TOKEN, server=VM_HOST, port=8080)

        if not self.blynk.connect():
            raise ConnectionError("Connection with blynk server failed")

    @retry(BaseException, delay=1, log_path=GLOBAL_PATH)
    def blynk_connect(self) -> None:
        """Blynk Connection Procedure"""
        self.__connect()

        # Updating relative up-time
        self.blynk.virtual_write(BLYNK_GLOBAL_MAP["rel_uptime"][0], datetime.now().strftime('%d/%m/%y %H:%M'))

    @retry(BrokenPipeError, tries=1, self_callback='blynk_connect')
    def update_properties(self, properties_dict: dict) -> None:
        for pin, inner_dict in properties_dict.items():
            self.blynk.set_property(pin, inner_dict["type"], inner_dict["prop"])

    @OneCallThread.threaded_function(thread_name='blynk_thread')
    @function_log_to_file(filename='main_run')
    @retry(BaseException, self_callback='blynk_connect', log_path=GLOBAL_PATH)
    @retry(ConnectionResetError, self_callback='blynk_connect')
    def main_run(self) -> None:  # Main blynk thread
        while self.BLYNK_THREAD_WHILE:
            self.blynk.run()

    @retry(BrokenPipeError, tries=1, self_callback='blynk_connect')
    def update_virtual_pin(self, value_dict: dict) -> None:
        for pin, value in value_dict.items():
            self.blynk.virtual_write(pin, value)


class BlynkTable(object):
    def __init__(self, ctx, pin: int, init_rows: list = None) -> None:
        # Initialize main parameters
        self.blynk, self.pin, self.struct = ctx.blynk, pin, []

        self.clear()
        if init_rows:
            self.add_rows(init_rows)

    def add_row(self, _id: int, name: str, value: str) -> None:
        """Adds a new row"""
        self.blynk.virtual_write(self.pin, "add", _id, name, value)

        self._add_element_to_struct(_id, name, value)

    def add_rows(self, rows: list) -> None:
        """Adds new rows format: rows = [[_id, name, value], ...]"""
        for row in rows:
            self.add_row(*row)

    @function_log_to_file()
    def clear_add_rows(self, rows: list) -> None:
        """Clear and Updates new rows format: rows = [[_id, name, value], ...]"""
        self.clear()
        self.add_rows(rows)

    @function_log_to_file()
    def update_rows(self, rows: list) -> None:
        """Updates new rows format: rows = [[_id, name, value], ...]"""
        for row in rows:
            self.update_row(*row)

    @function_log_to_file()
    def update_row(self, _id: int, name: str, value: str) -> None:
        """Updates a new row"""
        self.blynk.virtual_write(self.pin, "update", _id, name, value)

        self._update_element_to_struct(_id, name, value)

    def highlight_row(self, _id: int) -> None:
        """Highlights an item"""
        self.blynk.virtual_write(self.pin, "pick", _id)

    def select_row(self, _type: str, _id: int) -> None:
        """Selects or deselects an item"""
        assert _type in ["select", "deselect"]

        self.blynk.virtual_write(self.pin, _type, _id)

    def clear(self) -> None:
        """Clears the table"""
        self.blynk.virtual_write(self.pin, "clr")
        self.struct.clear()

    def _add_element_to_struct(self, _id: int, name: str, value: str) -> None:
        assert not self._search_in_struct(_id)
        self.struct.append({"_id": _id, "name": name, "value": value})

    def _update_element_to_struct(self, _id: int, name: str, value: str) -> None:
        if index := self._search_in_struct(_id):
            self.struct[index] = {"_id": _id, "name": name, "value": value}
        else:
            AssertionError("Item not found in table")

    def _search_in_struct(self, _id: int) -> int or None:
        return next((i for i, item in enumerate(self.struct) if item["_id"] == _id), None)


class BlynkHandler(BlynkMain):
    __counter: int = 0

    def __init__(self) -> None:
        # Check if __counter < = 1
        self.instances()

        # Init super-class
        super().__init__()

        # Init MeteoGram Table
        self.mg_table = BlynkTable(ctx=self, pin=METEOGRAM_BLYNK_TABLE_PIN, init_rows=MeteoGram.get_options())

        # If blynk connection succeeds
        self.__decorators_handler()

        # Updating historical data
        self.historical_update()
        self.blynk.virtual_write(50, datetime.now().strftime('%Y-%m-%d'))

        # Updating maps and radar
        self.update_radar_webcam_map()

        # Updating Daily forecast
        self._update_daily_forecast()

        # Updating Hourly forecast
        self.hourly_forecast_state: int = 0
        self._hourly_forecast_dict_updater()
        self._update_hourly_forecast(hour=self.hourly_forecast_state)

        # Updating Long-Range forecast
        self.update_long_range_forecast()

    @classmethod
    def instances(cls) -> None:
        cls.__counter += 1
        assert cls.__counter <= 1

    def __decorators_handler(self) -> None:
        """It handles periodical functions and blynk inputs"""

        @self.blynk.handle_event("write V50")
        def __historic_request_cmd_handler(_, value) -> None:
            if not value[0]:
                return

            self.historical_update(date=value[0].replace("-", ""))

        @self.blynk.handle_event("write V194")
        @retry(BaseException, tries=1, log_path=GLOBAL_PATH)
        def __meteogram_command_handler(_, value) -> None:
            if not value[0]:
                return

            if value[0] == "safe_url":
                self.blynk.virtual_write(194, MeteoGram.current_request)
                return
            elif value[0] == "pure_url":
                self.blynk.virtual_write(194, MeteoGram.get_pure_url())
                return
            elif value[0] == "new_request":
                self.blynk.virtual_write(194, "Wait...")
                MeteoGram.meteogram_request(force=True)
                self.blynk.virtual_write(194, "Done!")
                return
            elif value[0] == "update_table":
                self.blynk.virtual_write(194, "Wait...")
                self.mg_table.clear_add_rows(MeteoGram.get_options())
                self.blynk.virtual_write(194, "Done!")
                return

            if len(value_list := value[0].split("$")) != 3:
                self.blynk.virtual_write(194, f"Failed! Wrong format")
                return

            pin, path, val = value_list
            if pin != METEOGRAM_BLYNK_COMMAND_PIN or not path:
                self.blynk.virtual_write(194, f"Failed! Wrong pin '{pin}'")
                return

            key_string: str = "MeteoGram.current_options"  # WARNING
            for key in path.split(":"):
                key_string += f'["{key}"]' if not key.startswith("#") else f'[{int(key[1:])}]'

            try:
                from_value = eval(key_string)
            except KeyError:
                self.blynk.virtual_write(194, f"Failed! No Option '{path}' found")
                return

            if val == "&":
                self.blynk.virtual_write(194, f"Option '{path}' has value '{from_value}'")
                return

            if len(splitted_value := val.split(":")) != 2:
                self.blynk.virtual_write(194, f"Failed! You must specify a value type (probably '{type(from_value)}')")
                return

            _type, val = splitted_value

            key_string += f' = "{val}"' if _type == "str" else f' = {_type}({val})'
            try:
                exec(key_string)
            except NameError:
                self.blynk.virtual_write(194, f"Failed! Invalid type '{_type}")

            self.mg_table.update_rows(MeteoGram.get_options())
            JSONUtilities.save_json("meteogram.json", MeteoGram.current_options)

            self.blynk.virtual_write(194, f"Success! Option '{path}' updated from '{from_value}' to '{val}'")

        @self.blynk.handle_event("write V25")
        def __chosen_temperature_sensor_handler(_, value) -> None:
            self.temperature_sensor = TEMP_SENS_INDEX_MAP[value[0]]
            settings_dict = JSONUtilities.load_json("settings.json")
            settings_dict["main_temperature_sensor"] = self.temperature_sensor
            JSONUtilities.save_json("settings.json", settings_dict)

        @self.blynk.handle_event("write V202")
        def __chosen_current_forecast_handler(_, value) -> None:
            Abstract.current_forecast_provider = API_MAP[API_PROVIDERS_INDEX_MAP_CURRENT[value[0]]]
            settings_dict = JSONUtilities.load_json("settings.json")
            settings_dict["current_forecast"] = API_PROVIDERS_INDEX_MAP_CURRENT[value[0]]
            JSONUtilities.save_json("settings.json", settings_dict)

        @self.blynk.handle_event("write V203")
        def __chosen_hourly_forecast_handler(_, value) -> None:
            Abstract.hourly_forecast_provider = API_MAP[API_PROVIDERS_INDEX_MAP_HOURLY[value[0]]]
            settings_dict = JSONUtilities.load_json("settings.json")
            settings_dict["hourly_forecast"] = API_PROVIDERS_INDEX_MAP_HOURLY[value[0]]
            JSONUtilities.save_json("settings.json", settings_dict)
            self._hourly_forecast_dict_updater()  # Hourly
            self._update_hourly_forecast(hour=self.hourly_forecast_state)

        @self.blynk.handle_event("write V204")
        @function_log_to_file()
        def __chosen_daily_forecast_handler(_, value) -> None:
            Abstract.daily_forecast_provider = API_MAP[API_PROVIDERS_INDEX_MAP_DAILY[value[0]]]
            settings_dict = JSONUtilities.load_json("settings.json")
            settings_dict["daily_forecast"] = API_PROVIDERS_INDEX_MAP_DAILY[value[0]]
            JSONUtilities.save_json("settings.json", settings_dict)
            self._update_daily_forecast()  # Daily

        @self.blynk.handle_event("write V155")
        def __hourly_forecast_request_handler(_, value) -> None:
            self.hourly_forecast_state = int(value[0])
            self._update_hourly_forecast(hour=int(value[0]))

        @TimeEventHandler.absolute_time_event('hourly')
        def __historical_update() -> None:
            self.blynk.virtual_write(50, datetime.now().strftime('%Y-%m-%d'))
            self.historical_update(date=datetime.now().strftime('%Y%m%d'))

        @TimeEventHandler.relative_time_event(time=WEBCAM_INTERVAL)
        def __update_radar_webcam_map() -> None:
            self.update_radar_webcam_map()

        @function_log_to_file(filename='__update_forecast')
        @TimeEventHandler.relative_time_event(time=FORECAST_INTERVAL)
        def __update_forecast() -> None:
            self._update_daily_forecast()  # Daily
            self._hourly_forecast_dict_updater()  # Hourly
            self._update_hourly_forecast(hour=self.hourly_forecast_state)
            self.update_long_range_forecast()  # Long-Range Forecast

    @staticmethod
    def _recover_historical(date: str) -> (int, dict, str):
        if date != datetime.now().strftime('%Y%m%d'):
            try:
                HTTP_status_code, json_data = WeatherUnderground.get_historical(date)
            except TypeError:  # retry ended tries
                return True, {"errors": [{"message": "Unable to update historical data (UAHD)"}]}, ""

            try:
                assert HTTP_status_code == 200 and json_data.get("observations")
            except AssertionError:
                return True, json_data, ""

            json_data = json_data["observations"][0]
        else:
            try:
                HTTP_status_code, json_data = WeatherUnderground.get_seven_day()
            except TypeError:  # retry ended tries
                return True, {"errors": [{"message": "Unable to update historical data (UAHD)"}]}, ""

            try:
                assert HTTP_status_code == 200 and json_data.get("summaries")
            except AssertionError:
                return True, json_data, ""

            json_data = json_data["summaries"][-1]

        try:
            collected_data: dict = {**{param: json_data[param] if json_data[param] is not None else -500
                                       for param in ["humidityHigh", "humidityLow", "solarRadiationHigh", "uvHigh", "winddirAvg"]},
                                    **{param: json_data["metric"][param] if json_data["metric"][
                                                                                param] is not None else -500
                                       for param in
                                       ["tempHigh", "tempLow", "pressureMax", "pressureMin", "precipTotal", "dewptHigh",
                                        "dewptLow", "windgustHigh", "precipRate"]}}

            collected_data["winddirAvg"] = deg_to_compass(collected_data["winddirAvg"])
        except KeyError:
            return True, json_data, ""

        return False, collected_data, json_data.get("obsTimeLocal")

    @function_log_to_file(filename='historical_update')
    @retry(BrokenPipeError, tries=1, self_callback='blynk_connect')
    def historical_update(self, date: str = datetime.now().strftime('%Y%m%d')) -> None:
        there_was_an_error, data_dict, last_update_time = self._recover_historical(date)

        try:
            assert not there_was_an_error
            update_dict: dict = {key: value for key, value in data_dict.items()}
            self.update_value_procedure(HISTORICAL_FORMAT, **update_dict)
            self.update_properties_procedure(blynk.settings.color_settings.HISTORICAL_GRADIENT_MAP, [], {}, **update_dict)

            self.blynk.virtual_write(BLYNK_GLOBAL_MAP["historical_log"][0], f">> Last update: {last_update_time}")
        except AssertionError:
            if data_dict.get('errors') and data_dict['errors'][0].get('message'):
                self.blynk.virtual_write(BLYNK_GLOBAL_MAP["historical_log"][0],
                                         f">> {data_dict['errors'][0]['message']}")
                return
            self.blynk.virtual_write(BLYNK_GLOBAL_MAP["historical_log"][0], f">> Generic Error (NHDFE0)")

    # noinspection PyTypeChecker
    def blynk_update(self, **kwargs) -> None:
        """Procedure:
            1 - Update Value
            2 - Update Color
            3 - Update Urls
            4 - Update Label"""
        # Spare values
        kwargs['temp_0'] = kwargs['BME280_temperature_mean']
        kwargs['temp_1'] = kwargs['DS18B20_temperature_mean']
        kwargs['last_update'] = f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} (OA: {kwargs['dict_oneago']['when']})"

        reload(blynk.settings.label_settings)
        reload(blynk.settings.color_settings)
        self.update_value_procedure(BLYNK_VPINS_FORMAT_MAP, **kwargs)
        self.update_properties_procedure(blynk.settings.color_settings.GRADIENT_MAP, URLS_PARAMS, blynk.settings.label_settings.LABEL_MAP, **kwargs)

    def update_value_procedure(self, VPINS_FORMAT: dict, **kwargs) -> None:
        """It Updates values in BLYNK_VPINS_FORMAT_MAP"""
        try:
            self.update_virtual_pin({BLYNK_GLOBAL_MAP[param_key][i]: str_format % kwargs[param_key]
                                     for param_key, list_vpin in VPINS_FORMAT.items()
                                     for i, str_format in enumerate(list_vpin)})
        except TypeError as error:
            with open("type_error.txt", "a") as f:  # TODO: Debug try-catch (rimuovere una volta risolto il bug)
                f.write(f"{datetime.now().strftime('%d/%m/%Y - %H:%M:%S')}: \n{error}\n\n\n KWARGS: {kwargs}\n")
            raise TypeError("Controllare file type_error.txt.")

    def update_properties_procedure(self, GRADIENT: dict, URLS: list, LABEL: dict, **kwargs) -> None:
        """It updates colors, labels and urls"""
        # Color properties
        self.update_properties(
            {BLYNK_GLOBAL_MAP[param_key][0]: {"type": "color", "prop": get_gradient(kwargs[param_key], value)}
             for param_key, value in GRADIENT.items()})

        # Urls properties
        self.update_properties({BLYNK_GLOBAL_MAP[param_key][0]: {"type": "urls", "prop": kwargs[param_key]}
                                for param_key in URLS})

        # Labels properties
        for param_key, inner_dict in LABEL.items():  # TODO: Sfruttare update_properties
            oneago_string = ""
            if inner_dict.get("unit"):
                oneago_string = "  " + self.oneago_str(kwargs[param_key],
                                                       kwargs["dict_oneago"][param_key],
                                                       inner_dict["unit"])

            for i, check in enumerate(inner_dict["checks"]):
                if eval(check):  # Here we need kwargs
                    if inner_dict["standard"]:
                        self.blynk.set_property(BLYNK_GLOBAL_MAP[param_key][0], "label",
                                                f'{inner_dict["marks"][i]}'
                                                f'  {inner_dict["standard"]}{oneago_string}')
                    else:
                        self.blynk.set_property(BLYNK_GLOBAL_MAP[param_key][0], "label",
                                                f'{inner_dict["marks"][i]}'
                                                f'{oneago_string}')
                    break
            else:
                self.blynk.set_property(BLYNK_GLOBAL_MAP[param_key][0], "label",
                                        f'{inner_dict["standard"]}{oneago_string}')

    @staticmethod
    def oneago_str(value, value_OA, mes_unit) -> str:
        if (delta := round(value - value_OA, 2)) < 0:
            return f"▼  -{abs(delta)}{mes_unit}"
        elif delta > 0:
            return f"▲  +{abs(delta)}{mes_unit}"
        else:
            return f"▰  +{abs(delta)}{mes_unit}"

    @retry(BrokenPipeError, tries=3, self_callback='blynk_connect')
    def blynk_update_records(self, record_dict: dict) -> None:
        for record_type, params_dict in record_dict["records"].items():
            for param_key, inner_params_dict in params_dict.items():
                for key, value in inner_params_dict.items():
                    self.blynk.virtual_write(
                        RECORDS_VPINS_MAP["records"][record_type][param_key][key],
                        value if key == "data" else BLYNK_VPINS_FORMAT_MAP[param_key][-1] % value)

    @function_log_to_file(filename='update_radar_webcam_map')
    @retry(BrokenPipeError, tries=1, self_callback='blynk_connect')
    def update_radar_webcam_map(self) -> None:
        for args in (WEBCAM_ARGS, WEBCAMLONG_ARGS, RADAR_ARGS, OTHER_RADAR_ARGS, ALERTS_ARGS, MAP_ARGS):
            self.blynk.set_property(*args)

        for key, nested_dict in WRAM_VPINS_MAP.items():
            self.blynk.set_property(nested_dict["pin"], "label", f"{nested_dict['text']} 🗺️   "
                                                                 f"(Last update: {datetime.now().strftime('%d/%m/%Y %H:%M:%S')})")

    @function_log_to_file(filename='update_long_range_forecast')
    @retry(BrokenPipeError, tries=1, self_callback='blynk_connect')
    @retry((RequestConnectionError, ReadTimeout, IndexError), tries=3, delay=0.5, jitter=1)
    def update_long_range_forecast(self) -> None:
        for _, model_dict in LONG_RANGE_VPINS_MAP.items():
            link: str = 'https:' + BeautifulSoup(GET(model_dict["link"], timeout=10).content, 'html.parser').select(
                model_dict["selector"])[0].get("src")

            self.blynk.set_property(model_dict["pin"], "urls", link)
            self.blynk.set_property(model_dict["pin"], "label", f"{model_dict['text']} 🗺️   "
                                                                f"(Last update: {datetime.now().strftime('%d/%m/%Y %H:%M:%S')})")

    @function_log_to_file()
    def _update_daily_forecast(self) -> None:
        """Daily forecast handle"""
        if daily_list := Abstract.daily_forecast_provider.get_daily_weather(slice(0, 8), Abstract.hourly_forecast_provider.__name__):
            for i, day_dict in enumerate(daily_list):
                for key, value in day_dict.items():
                    self.__dict__[f"day{i}_{key}"] = value

    @function_log_to_file()
    def _update_hourly_forecast(self, hour: int = 0) -> None:
        """Hourly forecast updater"""
        if not hasattr(self, 'hour_forecast_list') or hour >= len(self.hour_forecast_list):  # Go ahead only if self has the forecast list in its own attributes
            return

        update_dict: dict = {f"hour_{key}": value for key, value in self.hour_forecast_list[hour].items()}
        self.update_value_procedure(HOURLY_FORECAST_FORMAT, **update_dict)
        self.update_properties_procedure(blynk.settings.color_settings.HOURLY_FORECAST_GRADIENT_MAP,
                                         URLS_HOURLY_FORECAST,
                                         blynk.settings.label_settings.HOURLY_FORECAST_LABEL_MAP,
                                         **update_dict)

    @function_log_to_file()
    def _hourly_forecast_dict_updater(self) -> None:
        """Hourly forecast updater"""
        if hourly_list := Abstract.hourly_forecast_provider.get_hourly_weather():
            self.hour_forecast_list: list = hourly_list

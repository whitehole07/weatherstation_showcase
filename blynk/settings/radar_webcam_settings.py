# GENERAL
WEBCAM_INTERVAL: int = 60

# VPINS
WRAM_VPINS_MAP: dict = {
    "webcam": {
        "pin": 13,
        "text": "Near Webcam"},
    "webcamlon": {
        "pin": 30,
        "text": "Webcam"},
    "radar": {
        "pin": 14,
        "text": "Radar"},
    "other_radar": {
        "pin": 199,
        "text": "Other Radar"},
    "alert": {
        "pin": 17,
        "text": "Warnings"},
    "map": {
        "pin": 31,
        "text": "Maps"}
}

# WEBCAM URL
NEAR_WEBCAM: dict = {
    'CHIETI': 'https://www.meteotorre.it/chieti.php',
    'PESCARA': 'https://www.pescarameteo.net/farewebcam.php',
    'AIRPORT': 'https://www.pescarameteo.net/farewebcam2.php',
    'COLLI': 'https://www.pescarameteo.net/farewebcam3.php',
    'TORREVECCHIA': 'https://www.meteotorre.it/farewebcam5.php',
    'TORREVECCHIA2': 'https://www.meteotorre.it/farewebcam2.php',
    'TORREVECCHIA3': 'https://www.meteotorre.it/farewebcam3.php',
    'TORREVECCHIA4': 'https://www.meteotorre.it/farewebcam.php',
    'TOLLO': 'https://www.meteotorre.it/tollo.php',
    'VALLEMARE': 'http://li1200.myfoscam.org:88/cgi-bin/CGIProxy.fcgi?cmd=snapPicture2&usr=visitor&pwd=visitor0',
    'CEPAGATTI': 'https://www.prowebcam.it/webcam/image/cepagatti2',
    'ORTONA': 'https://www.meteotorre.it/leganavale.php',
    'MONTESILVANO': 'http://www.torrevecchiameteo.it/montesilvano/cam.jpg',
}

FAR_WEBCAM: dict = {
    'POMILIO': 'http://www.torrevecchiameteo.it/pomilio/cam.jpg?t=1584826988',
    'MAJELLA': 'https://www.pescarameteo.net/maiella2/webcam.php',
    'MAJELLA2': 'https://www.pescarameteo.net/maiella/webcam.php',
    'PASSOLANCIANO1': 'https://tl.scenaridigitali.com/passolanciano/images/passolanciano.jpg',
    'PASSOLANCIANO2': 'https://www.ramsatwebcams.it/pl/upt360/images/panoramapark_01.jpg',
    'GRANSASSO': 'https://www.pescarameteo.net/gransasso/webcam.php',
    'GRANSASSO2': 'https://www.caputfrigoris.it/rete-meteo/prati/foscam/FI9900P_00626E8666D9/snap/webcam2.php',
    'GRANSASSO3': 'http://www.rifugioducadegliabruzzi.it/webcam/current2.jpg',
    'GRANSASSO4': 'https://www.caputfrigoris.it/rete-meteo/prati/madonnina/FI9900P_00626EA7D6EC/snap/webcam2.php',
    'ROCCARASO': 'http://www.centrometeomolise.it/zurrone/cam.jpg',
    'CAMPODIGIOVE1': 'https://www.caputfrigoris.it/rete-meteo/cdg/FI9900P_00626E9317F6/snap/webcam2.php',
    'CAMPODIGIOVE2': 'http://ipcam.witel.it/img/campodigiove/ipcam/03/hires/snapshot.jpg',
    'CALASCIO': 'https://www.caputfrigoris.it/rete-meteo/roccacalascio/foscam/FI9900P_00626E931802/snap/webcam2.php',
    'NAVELLI': 'https://www.caputfrigoris.it/rete-meteo/navelli/foscam/FI9900P_00626E6E75A2/snap/webcam2.php',
    'ROVERE': 'https://www.caputfrigoris.it/rete-meteo/rovere/foscam/FI9900P_00626E6E73F0/snap/webcam2.php',
    'PIETRACAMELA': 'https://images.webcamgalore.com/30544-current-webcam-Pietracamela.jpg',
}

# RADAR URL
RADAR: dict = {f'RADAR_{i}': f'http://radar.aquila.infn.it/radarcfa/Output/ultimo.{i}.png' for i in range(1, 14)}

# LIGHTNING URL
OTHER_RADAR: dict = {
    'RADAR_CEPA': 'http://radar.aquila.infn.it/cepagatti/images/ultimo.12.png',
    'RADAR_POH': 'http://radar.aquila.infn.it/radarcfa/Output/ultimo_pohv.png',
    'FULMINI_1': 'http://radar.aquila.infn.it/radarcfa/Output/ultimo_cfa_blitz.png',
    'FULMINI_2': 'http://radarweb.aquila.infn.it/cetempsite/fulmini.png'
}

# ALERT
ALERTS: dict = {
    'ALERT_1': 'https://www.meteoalarm.eu/map.php?iso=IT&data=0',
    'ALERT_2': 'https://www.meteoalarm.eu/map.php?iso=IT&data=1'
}

# MAPPE ITALIA
MAPS: dict = {
    'MAPPA_TEMP': 'http://cdn1.meteonetwork.it/models/malawi/wnetwork/mappe_realtime/realtime_temp_italia.png',
    'MAPPA_TEMP_A': 'http://cdn1.meteonetwork.it/models/malawi/wnetwork/mappe_realtime/realtime_temp_abruzzo.png',
    'MAPPA_DEW': 'http://cdn1.meteonetwork.it/models/malawi/wnetwork/mappe_realtime/realtime_dew_italia.png',
    'MAPPA_DEW_A': 'http://cdn1.meteonetwork.it/models/malawi/wnetwork/mappe_realtime/realtime_dew_abruzzo.png',
    'MAPPA_PRP': 'http://cdn1.meteonetwork.it/models/malawi/wnetwork/mappe_realtime/realtime_prec_italia.png',
    'MAPPA_PRP_A': 'http://cdn1.meteonetwork.it/models/malawi/wnetwork/mappe_realtime/realtime_prec_abruzzo.png',
    'MAPPA_HUMY': 'http://cdn1.meteonetwork.it/models/malawi/wnetwork/mappe_realtime/realtime_rh_italia.png',
    'MAPPA_HUMY_A': 'http://cdn1.meteonetwork.it/models/malawi/wnetwork/mappe_realtime/realtime_rh_abruzzo.png',
    'MAPPA_WIND': 'http://cdn1.meteonetwork.it/models/malawi/wnetwork/mappe_realtime/realtime_wind_italia.png',
    'MAPPA_WIND_A': 'http://cdn1.meteonetwork.it/models/malawi/wnetwork/mappe_realtime/realtime_wind_abruzzo.png',
    'MAPPA_PRESS': 'http://cdn1.meteonetwork.it/models/malawi/wnetwork/mappe_realtime/realtime_slp_italia.png',
    'MAPPA_PRESS_A': 'http://cdn1.meteonetwork.it/models/malawi/wnetwork/mappe_realtime/realtime_slp_abruzzo.png'
}

# READY ARGS
WEBCAM_ARGS: tuple = (WRAM_VPINS_MAP["webcam"]["pin"], "urls", *[url for _, url in NEAR_WEBCAM.items()])
WEBCAMLONG_ARGS: tuple = (WRAM_VPINS_MAP["webcamlon"]["pin"], "urls", *[url for _, url in FAR_WEBCAM.items()])
RADAR_ARGS: tuple = (WRAM_VPINS_MAP["radar"]["pin"], "urls", *[url for _, url in RADAR.items()])
OTHER_RADAR_ARGS: tuple = (WRAM_VPINS_MAP["other_radar"]["pin"], "urls", *[url for _, url in OTHER_RADAR.items()])
ALERTS_ARGS: tuple = (WRAM_VPINS_MAP["alert"]["pin"], "urls", *[url for _, url in ALERTS.items()])
MAP_ARGS: tuple = (WRAM_VPINS_MAP["map"]["pin"], "urls", *[url for _, url in MAPS.items()])

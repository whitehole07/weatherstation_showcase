from config import LAT, LON

# FORECAST INTERVALS
FORECAST_INTERVAL: int = 150

# VPINS
LONG_RANGE_VPINS_MAP: dict = {
    "GFS": {
        "pin": 172,
        "text": "GFS",
        "link": f"https://www.meteociel.fr/cartes_obs/gens_display.php?ext=1&lat={LAT}&lon={LON}&ville=",
        "selector": 'img[id="image"]'
    },
    "ECMWF#2": {
        "pin": 173,
        "text": "ECMWF",
        "link": f"https://www.meteociel.fr/cartes_obs/ecmwfens_display.php?lat={LAT}&lon={LON}&ville=",
        "selector": 'img[id="image"]'
    }
}

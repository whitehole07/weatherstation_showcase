# RECORDS V-MAP
RECORDS_VPINS_MAP: dict = {
  "records": {
    "MAX": {
      "temperature_mean": {
        "value": 102,
        "data": 103
      },
      "dew_point": {
        "value": 106,
        "data": 108
      },
      "pressure_mean": {
        "value": 113,
        "data": 114
      },
      "wind_gust": {
        "value": 109,
        "data": 110
      },
      "rain": {
        "value": 111,
        "data": 112
      },
      "rain_rate": {
        "value": 117,
        "data": 118
      },
      "snow": {
        "value": 195,
        "data": 196
      },
      "snow_rate": {
        "value": 197,
        "data": 198
      }
    },
    "MIN": {
      "temperature_mean": {
        "value": 101,
        "data": 104
      },
      "dew_point": {
        "value": 105,
        "data": 107
      },
      "pressure_mean": {
        "value": 115,
        "data": 116
      }
    }
  }
}

from others.abstract import Abstract

COLOR_UPPER_LIMIT: int = 185
COLOR_LOWER_LIMIT: int = 50

# GRADIENT MAPS
GRADIENT_MAP: dict = {
    "temperature_mean": (35, 20, 15, 5, -25, 'NORMAL'),
    "humidity_mean": (110, 80, 50, 20, -40, 'REVERSE§100'),  # -20
    "pressure_mean": (1060, 1035, 1010, 995, 950, 'NORMAL'),  # 1050, 1025
    "dew_point": (35, 20, 15, 5, -25, 'NORMAL'),
    "wind": (60, 30, 0, -5, -50, 'NORMAL'),
    "wind_gust": (80, 40, 0, -5, -50, 'NORMAL'),
    "rain_rate": (75, 15, 3.5, 1, -50, 'NORMAL'),
    "snow_rate": (30, 5, 2.5, 0.5, -50, 'NORMAL'),
    "feels_like": (35, 20, 15, 5, -25, 'NORMAL'),
    "wet_bulb": (35, 20, 15, 5, -25, 'NORMAL'),
    "cpu_temp": (100, 60, 30, 15, -10, 'NORMAL'),
    "visibility": (50000, 25000, 10000, 4000, -5000, 'NORMAL'),
    **{f"day{i}_temp_max": (35, 20, 15, 5, -25, 'NORMAL') for i in range(0, 8)},
    **{f"day{i}_temp_min": (35, 20, 15, 5, -25, 'NORMAL') for i in range(0, 8)},
    **{f"day{i}_wind_speed": (60, 30, 0, -5, -50, 'NORMAL') for i in range(0, 8)},
    **{f"day{i}_clouds": ((75, 15, 3.5, 1, -50, 'NORMAL') if Abstract.daily_forecast_provider.__name__ == "ClimaCell" else ()) for i in range(0, 8)},
}

HOURLY_FORECAST_GRADIENT_MAP: dict = {
    "hour_temp": (35, 20, 15, 5, -25, 'NORMAL'),
    "hour_dew_point": (35, 20, 15, 5, -25, 'NORMAL'),
    "hour_humidity": (110, 80, 50, 20, -40, 'REVERSE§100'),  # -20
    "hour_pressure": (1060, 1035, 1010, 995, 950, 'NORMAL'),  # 1050, 1025
    "hour_wind_speed": (60, 30, 0, -5, -50, 'NORMAL'),
    "hour_wet_bulb": (35, 20, 15, 5, -25, 'NORMAL')
}

HISTORICAL_GRADIENT_MAP: dict = {
    "tempHigh": (35, 20, 15, 5, -25, 'NORMAL'),
    "tempLow": (35, 20, 15, 5, -25, 'NORMAL'),
    "pressureMax": (1060, 1035, 1010, 995, 950, 'NORMAL'),  # 1050, 1025
    "pressureMin": (1060, 1035, 1010, 995, 950, 'NORMAL'),  # 1050, 1025
    "humidityHigh": (110, 80, 50, 20, -40, 'REVERSE§100'),  # -20
    "humidityLow": (110, 80, 50, 20, -40, 'REVERSE§100'),  # -20
    "dewptHigh": (35, 20, 15, 5, -25, 'NORMAL'),
    "dewptLow": (35, 20, 15, 5, -25, 'NORMAL'),
    "windgustHigh": (80, 40, 0, -5, -50, 'NORMAL'),
}

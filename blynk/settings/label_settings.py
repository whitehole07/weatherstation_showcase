from datetime import datetime, timedelta
from others.abstract import Abstract
from others.functions.solar_derivates import get_solar_percentage_variation_string

# LABEL MAPS
LABEL_MAP: dict = {
    "temperature_mean": {
        "standard": "Temperature 🌡️",
        "unit": "°C",
        "checks": ["kwargs['temperature_mean'] <= 0", "0 < kwargs['temperature_mean'] <= 5", "kwargs['temperature_mean'] >= 30"],
        "marks": ["(🧊)", "(❄️)", "(🔥)"]
    },
    "humidity_mean": {
        "standard": "Humidity 💧",
        "unit": "%",
        "checks": ["kwargs['humidity_mean'] <= 40",
                   "kwargs['humidity_mean'] >= 40 and kwargs['rain_rate'] != 0 and kwargs['dew_point'] > 0",
                   "kwargs['humidity_mean'] >= 40 and kwargs['rain_rate'] != 0 and kwargs['dew_point'] <= 0"],
        "marks": ["(🌵)", "(🌧️)", "(🌨️)"]
    },
    "pressure_mean": {
        "standard": "Barometer 🔰",
        "unit": "hPa",
        "checks": ["kwargs['pressure_mean'] < 1000"],
        "marks": ["(🌀)"]
    },
    "dew_point": {
        "standard": "Dew Point 💦",
        "unit": "°C",
        "checks": ["kwargs['dew_point'] <= 0 and kwargs['temperature_mean'] <= 5 and kwargs['humidity_mean'] >= 60"],
        "marks": ["(❄️)"]
    },
    "wind_gust": {
        "standard": "Wind Gust 🍃",
        "unit": "km/h",
        "checks": ["kwargs['wind_gust'] >= 40"],
        "marks": ["(⚠️)"]
    },
    "wind": {
        "standard": "Wind Speed 🍃",
        "unit": "km/h",
        "checks": ["kwargs['wind'] >= 25"],
        "marks": ["(⚠️)"]
    },
    "rain_rate": {
        "standard": "Rain Rate ☔",
        "unit": "mm/h",
        "checks": ["kwargs['rain_rate'] >= 15"],
        "marks": ["(⚠️)"]
    },
    "rain": {
        "standard": "Rain Today [MM] 🌧️",
        "unit": "mm",
        "checks": ["kwargs['rain'] >= 50"],
        "marks": ["(⚠️)"]
    },
    "snow": {
        "standard": f"Snow Today [CM] 🌨️",
        "unit": "cm",
        "checks": ["kwargs['snow'] >= 15"],
        "marks": ["(⚠️)"]
    },
    "snow_rate": {
        "standard": "Snow Rate ☔",
        "unit": "cm/h",
        "checks": ["kwargs['snow_rate'] >= 5"],
        "marks": ["(⚠️)"]
    },
    "wet_bulb": {
        "standard": "Wet Bulb 💦",
        "unit": "°C",
        "checks": ["kwargs['wet_bulb'] <= 0 and kwargs['temperature_mean'] <= 5 and kwargs['humidity_mean'] >= 60"],
        "marks": ["(☃️)"]
    },
    "cloud_base": {
        "standard": "Cloud Base ☁️",
        "unit": "mt",
        "checks": [f"kwargs['cloud_base'] <= 100"],
        "marks": ["(🌁)"]
    },
    "feels_like": {
        "standard": "Feels Like 🌡️",
        "unit": "°C",
        "checks": ["kwargs['feels_like'] <= 0", "kwargs['feels_like'] >= 40"],
        "marks": ["(🥶)", "(🥵)"]
    },
    "uv_index": {
        "standard": "UV Index ☀️",
        "unit": " ",
        "checks": ["kwargs['uv_index'] >= 6"],
        "marks": ["(⚠️)"]
    },
    "solar_radiation": {
        "standard": "Solar Radiation ⛅",
        "unit": "W/m²",
        "checks": ["kwargs['solar_radiation'] >= 600"],
        "marks": ["(⚡)"]
    },
    "solar_percentage": {
        "standard": f"Sun [%] ☀  {get_solar_percentage_variation_string(2, Abstract.current_forecast_provider.get_sunrise(), Abstract.current_forecast_provider.get_sunset())}",
        "checks": ["False"]
    },
    "cpu_temp": {
        "standard": "CPU TEMPERATURE 🌡️",
        "unit": "°C",
        "checks": ["kwargs['cpu_temp'] >= 70"],
        "marks": ["(⚠️)"]
    },
    "visibility": {
            "standard": "Visibility ☴",
            "unit": "mt",
            "checks": ["kwargs['visibility'] <= 2000"],
            "marks": ["(⚠️)"]
        },
    "cloudiness": {
            "standard": "Cloudiness 🌤️",
            "unit": "%",
            "checks": ["kwargs['cloudiness'] == 100"],
            "marks": ["(☁️)"]
        },
    "exp_prep": {
                "standard": f"",
                "checks": ["kwargs['exp_prep_flag'] == 'minute' and kwargs['exp_prep'] >= 5", "kwargs['exp_prep_flag'] == 'minute'",
                           "kwargs['exp_prep_flag'] == 'hour' and kwargs['exp_prep'] >= 25", "kwargs['exp_prep_flag'] == 'hour'",
                           "kwargs['exp_prep_flag'] == 'now' and kwargs['exp_prep'] >= 15", "kwargs['exp_prep_flag'] == 'now'",
                            "kwargs['exp_prep_flag'] == 'minutecc' and kwargs['exp_prep'] >= 10", "kwargs['exp_prep_flag'] == 'minutecc'",
                           "kwargs['exp_prep_flag'] == 'hourcc' and kwargs['exp_prep'] >= 50", "kwargs['exp_prep_flag'] == 'hourcc'"],
                "marks": [f"(⚠️) ({datetime.now().strftime('%H:%M')} - {(datetime.now() + timedelta(hours=1)).strftime('%H:%M')})  Expected Precipitation [MM] 🌧️/🌨️",
                          f"({datetime.now().strftime('%H:%M')} - {(datetime.now() + timedelta(hours=1)).strftime('%H:%M')})  Expected Precipitation [MM] 🌧️/🌨️",
                          f"(⚠️) ({datetime.now().strftime('%d/%m H %H')} - {(datetime.now() + timedelta(hours=48)).strftime('%d/%m H %H')})  Expected Precipitation [MM] 🌧️/🌨️",
                          f"({datetime.now().strftime('%d/%m H %H')} - {(datetime.now() + timedelta(hours=48)).strftime('%d/%m H %H')})  Expected Precipitation [MM] 🌧️/🌨️",
                          f"(⚠️) (NOW)  Observed Precipitation Rate [MM/H] 🌧️/🌨️", f"(NOW)  Observed Precipitation Rate [MM/H] 🌧️/🌨️",
                          f"(⚠️) ({datetime.now().strftime('%H:%M')} - {(datetime.now() + timedelta(minutes=360)).strftime('%H:%M')})  Expected Precipitation [MM] 🌧️/🌨️",
                          f"({datetime.now().strftime('%H:%M')} - {(datetime.now() + timedelta(minutes=360)).strftime('%H:%M')})  Expected Precipitation [MM] 🌧️/🌨️",
                          f"(⚠️) ({datetime.now().strftime('%d/%m H %H')} - {(datetime.now() + timedelta(hours=108)).strftime('%d/%m H %H')})  Expected Precipitation [MM] 🌧️/🌨️",
                          f"({datetime.now().strftime('%d/%m H %H')} - {(datetime.now() + timedelta(hours=108)).strftime('%d/%m H %H')})  Expected Precipitation [MM] 🌧️/🌨️"]
            },
    **{f"day{i}_pop": {
        "standard": f"Probability of precipitation 🌦️",
        "checks": [f"kwargs['day{i}_pop'] >= 70"],
        "marks": ["(☔)"]
    } for i in range(0, 8)},
    **{f"day{i}_rain": {
        "standard": f"",
        "checks": [f"kwargs['day{i}_rain']", "True"],
        "marks": ["💧[MM]", "None"]
    } for i in range(0, 8)},
    **{f"day{i}_snow": {
        "standard": f"",
        "checks": [f"kwargs['day{i}_snow']", "True"],
        "marks": ["❄[CM]", "None"]
    } for i in range(0, 8)},
    **{f"day{i}_wind_speed": {
        "standard": "Wind Speed 🍃",
        "checks": [f"kwargs['day{i}_wind_speed'] >= 25"],
        "marks": ["(⚠️)"]
    } for i in range(0, 8)},
    **{f"day{i}_temp_max": {
        "standard": "Max Temperature ↗️",
        "checks": [f"kwargs['day{i}_temp_max'] <= 0", f"0 < kwargs['day{i}_temp_max'] <= 5", f"kwargs['day{i}_temp_max'] >= 30"],
        "marks": ["(🧊)", "(❄️)", "(🔥)"]
    } for i in range(0, 8)},
    **{f"day{i}_temp_min": {
        "standard": "Min Temperature ↘️",
        "checks": [f"kwargs['day{i}_temp_min'] <= 0", f"0 < kwargs['day{i}_temp_min'] <= 5", f"kwargs['day{i}_temp_min'] >= 30"],
        "marks": ["(🧊)", "(❄️)", "(🔥)"]
    } for i in range(0, 8)},
    **{f"day{i}_weather": {
        "standard": "Weather" + (f"  ({Abstract.daily_forecast_provider.get_daily_header(i)})" if Abstract.daily_forecast_provider.get_daily_header(i) else ""),
        "checks": ["False"]
    } for i in range(0, 8)}
}

HOURLY_FORECAST_LABEL_MAP: dict = {
    "hour_temp": {
        "standard": "Temperature 🌡️",
        "checks": ["kwargs['hour_temp'] <= 0", "0 < kwargs['hour_temp'] <= 5", "kwargs['hour_temp'] >= 30"],
        "marks": ["(🧊)", "(❄️)", "(🔥)"]
    },
    "hour_pop": {
        "standard": f"Probability of precipitation 🌦️ [%]",
        "checks": [f"kwargs['hour_pop'] >= 70"],
        "marks": ["(☔)"]
    },
    "hour_rain": {
        "standard": f"",
        "checks": [f"kwargs['hour_rain']", "True"],
        "marks": ["💧[MM]", "None"]
    },
    "hour_snow": {
        "standard": f"",
        "checks": [f"kwargs['hour_snow']", "True"],
        "marks": ["❄[CM]", "None"]
    },
    "hour_wind_speed": {
        "standard": "Wind Speed 🍃",
        "checks": ["kwargs['hour_wind_speed'] >= 25"],
        "marks": ["(⚠️)"]
    },
}

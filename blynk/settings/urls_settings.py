# URLS PARAMS
URLS_PARAMS: list = ["cc_icon_url",
                     *[f"day{i}_weather_icon_url" for i in range(0, 8)],
                     "meteogram_url"]

URLS_HOURLY_FORECAST: list = ["hour_weather_icon_url"]

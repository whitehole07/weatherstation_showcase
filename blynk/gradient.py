from blynk.settings.color_settings import COLOR_UPPER_LIMIT, COLOR_LOWER_LIMIT


def get_gradient(value, grad_conf_list: tuple) -> str:
    if not grad_conf_list:
        return "#d6d6d6"

    value_RR, value_RG, value_GG, value_GB, value_BB, option = grad_conf_list

    if option.startswith('REVERSE'):
        _, rev_value = option.split('§')
        value = float(rev_value) - value  # FLOAT

    def calc_perc(passed, valueMIN, valueMAX):
        return abs(((passed - valueMIN) / (valueMAX - valueMIN)))  # Percentuale nell'intervallo

    if value > value_RR:  # Se valore > valore Rosso
        RED = str(hex(COLOR_UPPER_LIMIT)).replace('0x', '').upper()
        GREEN = str(hex(COLOR_LOWER_LIMIT)).replace('0x', '').upper()
        BLUE = str(hex(COLOR_LOWER_LIMIT)).replace('0x', '').upper()
        HEX = '#' + RED + GREEN + BLUE
    elif value_RR >= value > value_RG:  # Se valore Rosso < valore < valore Giallo
        perc = calc_perc(value, value_RR, value_RG)
        RED = str(hex(COLOR_UPPER_LIMIT)).replace('0x', '').upper()
        GREEN = str(hex(int(perc * COLOR_UPPER_LIMIT))).replace('0x', '').upper()
        if int(perc * COLOR_UPPER_LIMIT) <= 16:
            GREEN = '0' + GREEN
        BLUE = str(hex(COLOR_LOWER_LIMIT)).replace('0x', '').upper()
        HEX = '#' + RED + GREEN + BLUE
    elif value_RG >= value > value_GG:  # Se valore Giallo < valore < valore Verde
        perc = calc_perc(value, value_GG, value_RG)
        RED = str(hex(int(perc * COLOR_UPPER_LIMIT))).replace('0x', '').upper()
        if int(perc * COLOR_UPPER_LIMIT) <= 16:
            RED = '0' + RED
        GREEN = str(hex(COLOR_UPPER_LIMIT)).replace('0x', '').upper()
        BLUE = str(hex(COLOR_LOWER_LIMIT)).replace('0x', '').upper()
        HEX = '#' + RED + GREEN + BLUE
    elif value_GG >= value > value_GB:  # Se valore Verde < valore < valore Ciano
        perc = calc_perc(value, value_GG, value_GB)
        RED = str(hex(COLOR_LOWER_LIMIT)).replace('0x', '').upper()
        GREEN = str(hex(COLOR_UPPER_LIMIT)).replace('0x', '').upper()
        BLUE = str(hex(int(perc * COLOR_UPPER_LIMIT))).replace('0x', '').upper()
        if int(perc * COLOR_UPPER_LIMIT) <= 16:
            BLUE = '0' + BLUE
        HEX = '#' + RED + GREEN + BLUE
    elif value_GB >= value >= value_BB:  # Se valore Ciano < valore < valore Blu
        perc = calc_perc(value, value_BB, value_GB)
        RED = str(hex(COLOR_LOWER_LIMIT)).replace('0x', '').upper()
        GREEN = str(hex(int(perc * COLOR_UPPER_LIMIT))).replace('0x', '').upper()
        if int(perc * COLOR_UPPER_LIMIT) <= 16:
            GREEN = '0' + GREEN
        BLUE = str(hex(COLOR_UPPER_LIMIT)).replace('0x', '').upper()
        HEX = '#' + RED + GREEN + BLUE
    elif value < value_BB:  # Se valore < valore Blu
        RED = str(hex(COLOR_LOWER_LIMIT)).replace('0x', '').upper()
        GREEN = str(hex(COLOR_LOWER_LIMIT)).replace('0x', '').upper()
        BLUE = str(hex(COLOR_UPPER_LIMIT)).replace('0x', '').upper()
        HEX = '#' + RED + GREEN + BLUE
    else:
        HEX = '#cfcfcf'

    return HEX

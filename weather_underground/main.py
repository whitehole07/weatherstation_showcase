from time import strftime, time
from requests import get as GET
from requests.exceptions import ConnectionError as RequestConnectionError
from requests.exceptions import ReadTimeout

from configs.wu_config import WU_STATION_ID, WU_STATION_PWD, WU_APIKEY, \
    WU_ADIACENT_STATION_ID, PER_DAY_API_CURRENT_REQUESTS
from utilities.unit_conversion import UnitConversion
from utilities.decorator_error_handler import retry
from json.decoder import JSONDecodeError


class WeatherUnderground(object):
    current_request: dict = {}
    last_api_request: float = 0.0

    @classmethod
    @retry((RequestConnectionError, ReadTimeout), tries=3, delay=0.5, jitter=1)
    def wu_upload(cls, **kwargs) -> None:
        """Weather Data Uploads on WeatherUnderground PWS Network"""
        WUurl: str = "https://rtupdate.wunderground.com/weatherstation/updateweatherstation.php?"
        WUcreds: str = "ID=" + WU_STATION_ID + "&PASSWORD=" + WU_STATION_PWD
        date_str: str = "&dateutc=now"
        action_str: str = "&action=updateraw&realtime=1&rtfreq=1"

        GET(
            WUurl +
            WUcreds +
            date_str +
            f"&humidity={kwargs['humidity_mean']}"
            f"&baromin={UnitConversion.hpa_to_inches(kwargs['pressure_mean'])}"
            f"&tempf={UnitConversion.celsius_to_farenheit(kwargs['temperature_mean'])}"
            f"&dewptf={UnitConversion.celsius_to_farenheit(kwargs['dew_point'])}"
            f"&dailyrainin={UnitConversion.mm_to_inches(kwargs['rain'])}"
            f"&rainin={UnitConversion.mm_to_inches(kwargs['rain_rate'])}"
            f"&windspeedmph={UnitConversion.kph_to_mph(kwargs['wind'])}"
            f"&winddir={kwargs['wind_dir_degree']}"
            f"&windgustmph={UnitConversion.kph_to_mph(kwargs['wind_gust'])}"
            f"&solarradiation={kwargs['solar_radiation']}"
            f"&uv={kwargs['uv_index']}" +
            action_str,
            timeout=10)

    @classmethod
    @retry((RequestConnectionError, ReadTimeout), tries=3, delay=0.5, jitter=1)
    def __api_request_current(cls) -> None:
        if (time() - cls.last_api_request) >= ((24 * 60 * 60) / PER_DAY_API_CURRENT_REQUESTS):
            r = GET(f"https://api.weather.com/v2/pws/observations/current?station"
                    f"Id={WU_ADIACENT_STATION_ID}&format=json&units=m&apiKey={WU_APIKEY}",
                    timeout=10)

            try:
                assert r.status_code == 200
                cls.current_request = r.json()
                cls.last_api_request = time()
            except AssertionError:  # If i can't recover those data
                cls.current_request = {
                    "observations": {
                        0: {
                            "solarRadiation": 0,
                            "uv": 0
                        }
                    }
                }

    @staticmethod
    @retry((RequestConnectionError, ReadTimeout), tries=10, delay=1, jitter=1)
    def wu_rain_recover() -> int:
        r = GET(f"https://api.weather.com/v2/pws/observations/current?station"
                f"Id={WU_STATION_ID}&format=json&units=m&apiKey={WU_APIKEY}",
                timeout=10)

        try:
            assert r.status_code == 200
            json_r = r.json()
        except AssertionError:  # If i can't recover that data
            return 0

        if (json_r["observations"][0]["obsTimeLocal"])[:10] == strftime("%Y-%m-%d"):
            return int(round(json_r["observations"][0]["metric"]["precipTotal"] / 0.2794))

        return 0

    @classmethod
    def get_solar(cls) -> float:
        cls.__api_request_current()

        return cls.current_request["observations"][0]["solarRadiation"] if cls.current_request["observations"][0]["solarRadiation"] is not None else 0.0

    @classmethod
    def get_uvindex(cls) -> float:
        cls.__api_request_current()

        return cls.current_request["observations"][0]["uv"] if cls.current_request["observations"][0]["uv"] is not None else 0.0

    @staticmethod
    @retry((RequestConnectionError, ReadTimeout), tries=3, delay=0.5, jitter=1)
    def get_historical(date: str) -> (int, dict):
        r = GET(f"https://api.weather.com/v2/pws/history/daily?station"
                f"Id={WU_STATION_ID}&format=json&units=m&date={date}"
                f"&apiKey={WU_APIKEY}&numericPrecision=decimal",
                timeout=10)

        try:
            json_data: dict = r.json()
        except JSONDecodeError:
            json_data: dict = {}

        return r.status_code, json_data

    @staticmethod
    @retry((RequestConnectionError, ReadTimeout), tries=3, delay=0.5, jitter=1)
    def get_seven_day() -> (int, dict):
        r = GET(f"https://api.weather.com/v2/pws/dailysummary/7day?station"
                f"Id={WU_STATION_ID}&format=json&units=m&apiKey={WU_APIKEY}&numericPrecision=decimal",
                timeout=10)

        try:
            json_data: dict = r.json()
        except JSONDecodeError:
            json_data: dict = {}

        return r.status_code, json_data

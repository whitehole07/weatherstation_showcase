from datetime import datetime
from gpiozero import CPUTemperature

from jsons.json_utilities import JSONUtilities
from config import WEATHER_STATION_INTERVAL, GLOBAL_PATH
from blynk.settings.value_settings import BLYNK_VPINS_FORMAT_MAP
from blynk.main import BlynkHandler
from sensors.bme280 import BME280
from sensors.rain import RainGauge
from sensors.snow import SnowGauge
from sensors.wind import WindSpeedGauge, WindDirectionGauge
from sensors.fan import ActiveFan
from sensors.ds18b20 import DS18B20
from others.derived_parameters import DerivedParameters
from weather_underground.main import WeatherUnderground
from pws_weather.main import PWS_Weather
from utilities.date_event import TimeEventHandler, remove_by_time_from_list_of_dicts
from utilities.threading_utilities import LoopHandler
from utilities.operators import Operators
from utilities.error_logger import function_log_to_file
from utilities.decorator_error_handler import retry
from blynk.settings.label_settings import LABEL_MAP


class WeatherStation(WindSpeedGauge, WindDirectionGauge, RainGauge, SnowGauge, ActiveFan, BME280, DS18B20, DerivedParameters, BlynkHandler):
    def __init__(self) -> None:
        """Init the WeatherStation instance"""
        super(WeatherStation, self).__init__()
        super(WindSpeedGauge, self).__init__()
        super(WindDirectionGauge, self).__init__()
        super(RainGauge, self).__init__()
        super(SnowGauge, self).__init__()
        super(ActiveFan, self).__init__()
        super(BME280, self).__init__()

        # Get the right temperature sensor
        self.temperature_sensor: str = JSONUtilities.load_json("settings.json")["main_temperature_sensor"]

        # Get the right sensor
        self._which_sensor()

        # Init Derived Parameters
        super(DS18B20, self).__init__(self)

        # Init Blynk Class
        super(DerivedParameters, self).__init__()

        # Get CPU Temperature
        self.cpu_temp = CPUTemperature().temperature

        # Get Records dict
        self.records_dict = JSONUtilities.load_json("records.json")

        # Blynk Update Records
        self.blynk_update_records(self.records_dict)

        # One-Ago Init procedure
        self.list_dict_oneago: list = []
        self.dict_oneago: dict = {}
        self.oneago_update()

        # Decorator Events
        self.__time_events()

    def __time_events(self) -> None:
        """It manages time-based event where 'self' is required"""
        @TimeEventHandler.relative_time_event(time=WEATHER_STATION_INTERVAL)
        def __oneago_update() -> None:
            """It calls the oneago_update function every WEATHER_STATION_INTERVAL seconds"""
            self.oneago_update()

        @LoopHandler.main_loop(wait_between_cycles=WEATHER_STATION_INTERVAL)
        def __loop_handler() -> None:
            """Loop handler's decorator function"""
            self.__loop()

    @function_log_to_file()
    def oneago_update(self) -> None:
        """It Updates One Hour ago data"""
        self.list_dict_oneago.append({**{key: self.__dict__[key] for key in [key for key, _ in LABEL_MAP.items() if _.get("unit")]},
                                      "time": datetime.now().timestamp(), "when": datetime.now().strftime("%H:%M")})

        self.list_dict_oneago = remove_by_time_from_list_of_dicts(self.list_dict_oneago, relative_time=3600)  # Removes all elements older than relative_time seconds
        self.dict_oneago = self.list_dict_oneago[0]  # Set the oneago dict equals to the dicts list's first element

    def _which_sensor(self) -> None:
        """It gets the current selected sensor (from blynk)"""
        self.temperature_mean = self.__dict__[f"{self.temperature_sensor}_temperature_mean"]

    def check_records(self) -> None:
        """It looks for any record"""
        operator_map: dict = {
            'MAX': Operators.greater,
            'MIN': Operators.lower
        }

        for record_type, params_dict in self.records_dict["records"].items():
            for param_key, inner_params_dict in params_dict.items():
                if operator_map[record_type](self.__dict__[param_key], inner_params_dict["value"]):
                    self.records_dict["records"][record_type][param_key]["value"] = float(BLYNK_VPINS_FORMAT_MAP[param_key][-1] % self.__dict__[param_key])
                    self.records_dict["records"][record_type][param_key]["data"] = datetime.now().strftime('%d/%m/%Y %H:%M')
                    self.blynk_update_records({"records": {record_type: {param_key: self.records_dict["records"][record_type][param_key]}}})
                    JSONUtilities.save_json("records.json", self.records_dict)

    @retry(BaseException, delay=1, log_path=GLOBAL_PATH)
    def __loop(self) -> None:
        """This is the main loop"""
        # Get CPU Temperature, used for monitoring
        self.cpu_temp = CPUTemperature().temperature

        # Get the right sensor
        self._which_sensor()

        # Update derived parameters
        self.update_derived_parameters()

        # Update Active ventilation fan state
        self.fan_set(self)

        # WeatherUnderground data update
        WeatherUnderground.wu_upload(**self.__dict__)

        # PWSWeather data update
        PWS_Weather.pws_upload(**self.__dict__)

        # Blynk data update
        self.blynk_update(**self.__dict__)

        # Check records
        self.check_records()

    @staticmethod
    def run_loop() -> None:
        """This runs the main loop"""
        LoopHandler.executor_loop()

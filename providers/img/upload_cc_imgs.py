import base64
import requests
import json
from time import sleep

# Make dict
xx2 = ['200d', '201d', '202d', '210d', '211d', '212d', '221d', '230d', '231d', '232d', '200n', '201n', '202n', '210n', '211n', '212n', '221n', '230n', '231n', '232n']
xx3 = ['300d', '301d', '302d', '310d', '311d', '312d', '313d', '314d', '321d', '300n', '301n', '302n', '310n', '311n', '312n', '313n', '314n', '321n']
xx5 = ['500d', '501d', '502d', '503d', '504d', '511d', '520d', '521d', '522d', '531d', '500n', '501n', '502n', '503n', '504n', '511n', '520n', '521n', '522n', '531n']
xx6 = ['600d', '601d', '602d', '611d', '612d', '613d', '615d', '616d', '620d', '621d', '622d', '623d', '600n', '601n', '602n', '611n', '612n', '613n', '615n', '616n', '620n', '621n', '622n', '623n']
xx7 = ['701d', '711d', '721d', '731d', '741d', '751d', '761d', '762d', '771d', '781d', '701n', '711n', '721n', '731n', '741n', '751n', '761n', '762n', '771n', '781n']
xx8 = ['800d', '801d', '802d', '803d', '804d', '800n', '801n', '802n', '803n', '804n']

from_dict: dict = {}
for num_list in [xx2, xx3, xx5, xx6, xx7, xx8]:
    for num in num_list:
        from_dict[num] = f"{num[0]}xx/{num[-1]}/{num[:-1]}.png"
print(from_dict)

link_dict: dict = {}
for num, path in from_dict.items():
    with open(path, "rb") as file:
        url = "https://api.imgbb.com/1/upload"
        payload = {
            "key": "key",
            "image": base64.b64encode(file.read()),
            "name": num,
        }
        res = requests.post(url, payload)
        s = res.json()
        link_dict[num] = s["data"]["url"]
        print(res.status_code)
        sleep(0.5)

print(link_dict)
with open("../../jsons/json/code_to_icon.json", "w") as file:
    json.dump(link_dict, file, ensure_ascii=False, indent=2)

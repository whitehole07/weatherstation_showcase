from requests import get as GET
from datetime import datetime, timezone
from requests.exceptions import ConnectionError as RequestConnectionError
from requests.exceptions import ReadTimeout

from configs.providers.wb_config import API_KEY, PER_DAY_API_DAILY_REQUESTS, \
    PER_DAY_API_CURRENT_REQUESTS, CUSTOM_WEATHER_ICON_CONDITIONS, PORT, MAIN_DICT
from config import LAT, LON
from others.functions.deg_to_compass import deg_to_compass
from others.functions.get_weather_icon import get_weather_icon
from others.functions.get_weather_main import get_main_weather
from utilities.decorator_error_handler import retry


class WeatherBit(object):
    # CURRENT
    PER_DAY_API_CURRENT_REQUESTS = PER_DAY_API_CURRENT_REQUESTS
    current_request: dict = {}
    last_api_current_request: float = 0.0

    # DAILY
    PER_DAY_API_DAILY_REQUESTS = PER_DAY_API_DAILY_REQUESTS
    daily_request: dict = {}
    last_api_daily_request: float = 0.0

    @classmethod
    def get_current_conditions(cls) -> (str, str, str):
        cls.__api_request_current()

        return get_main_weather(MAIN_DICT, cls.current_request["data"][0]["weather"]["code"]).capitalize(), \
               cls.current_request["data"][0]["weather"]["description"].capitalize(), \
               get_weather_icon(CUSTOM_WEATHER_ICON_CONDITIONS, cls.get_cloudiness(),
                                PORT[cls.current_request["data"][0]["weather"]["code"]] if cls.current_request["data"][0]["weather"]["code"] in PORT.keys() else cls.current_request["data"][0]["weather"]["code"],
                                "d" if cls.get_sunrise() <= datetime.now(cls.get_sunrise().tzinfo) < cls.get_sunset() else "n")

    @classmethod
    def get_current_time(cls) -> float:
        cls.__api_request_current()

        return datetime.fromtimestamp(cls.current_request["data"][0]["ts"]).replace(tzinfo=timezone.utc).astimezone(tz=None).timestamp()

    @classmethod
    def get_visibility(cls) -> int:
        cls.__api_request_current()

        return cls.current_request["data"][0]["vis"] * 1000  # Default km

    @classmethod
    def get_cloudiness(cls) -> int:
        cls.__api_request_current()

        return cls.current_request["data"][0]["clouds"]

    @classmethod
    def get_sunrise(cls) -> datetime:
        cls.__api_request_current()

        return datetime.strptime(datetime.now().strftime("%d %m %Y ") + cls.current_request["data"][0]["sunrise"], "%d %m %Y %H:%M").replace(tzinfo=timezone.utc).astimezone(tz=None) if cls.current_request.get("data") else None

    @classmethod
    def get_sunset(cls) -> datetime:
        cls.__api_request_current()

        return datetime.strptime(datetime.now().strftime("%d %m %Y ") + cls.current_request["data"][0]["sunset"], "%d %m %Y %H:%M").replace(tzinfo=timezone.utc).astimezone(tz=None) if cls.current_request.get("data") else None

    @classmethod
    def get_snow(cls) -> float:
        cls.__api_request_current()

        return cls.current_request["data"][0]["snow"] / 10

    @classmethod
    def get_precipitation(cls, *args) -> (str, float, str):
        return "Liquid precipitation rate: %.2f mm/h" % cls.current_request["data"][0]["precip"], cls.current_request["data"][0]["precip"], "now"

    @classmethod
    def get_dt_current(cls) -> float:
        return (24 * 60 * 60) / cls.PER_DAY_API_CURRENT_REQUESTS

    @classmethod
    @retry((RequestConnectionError, ReadTimeout), tries=3, delay=0.5, jitter=1)
    def __api_request_current(cls) -> None:
        if (datetime.now().timestamp() - cls.last_api_current_request) >= cls.get_dt_current():
            r = GET(f"https://api.weatherbit.io/v2.0/current?lat={LAT}&lon={LON}&key={API_KEY}",
                    timeout=10)

            try:
                assert r.status_code == 200
                current_request: dict = r.json()

                cls.current_request, cls.last_api_current_request = current_request, datetime.now().timestamp()
            except AssertionError:  # If i can't recover those data
                pass

    @classmethod
    def get_daily_weather(cls, from_to: slice, *args) -> list:
        cls.__api_request_daily()

        if not cls.daily_request.get("data"):
            return []

        return [
            {
                "when": datetime.fromtimestamp(day["ts"]).strftime("%A %d %B, %Y"),
                "temp_max": day["max_temp"],
                "temp_min": day["min_temp"],
                "weather": f'{get_main_weather(MAIN_DICT, day["weather"]["code"])}, {day["weather"]["description"].lower()}',
                "weather_icon_url": get_weather_icon(CUSTOM_WEATHER_ICON_CONDITIONS, day["clouds"],
                                                     PORT[day["weather"]["code"]] if day["weather"]["code"] in PORT.keys() else day["weather"]["code"],
                                                     day["weather"]["icon"][-1]),
                "pop": day["pop"],
                "rain": day["precip"],
                "snow": day["snow"] / 10,
                "wind_speed": day["wind_spd"] * 3.6,
                "wind_dir": deg_to_compass(day["wind_dir"]),
                "clouds": day["clouds"]
            } for i, day in enumerate(cls.daily_request["data"][from_to])
        ]

    @classmethod
    def get_daily_header(cls, _: int) -> str:
        return ""  # Not implemented yet

    @classmethod
    def get_dt_daily(cls) -> float:
        return (24 * 60 * 60) / cls.PER_DAY_API_DAILY_REQUESTS

    @classmethod
    @retry((RequestConnectionError, ReadTimeout), tries=3, delay=0.5, jitter=1)
    def __api_request_daily(cls) -> None:
        if (datetime.now().timestamp() - cls.last_api_daily_request) >= cls.get_dt_daily():
            r = GET(f"https://api.weatherbit.io/v2.0/forecast/daily?lat={LAT}&lon={LON}&key={API_KEY}",
                    timeout=10)

            try:
                assert r.status_code == 200
                daily_request: dict = r.json()

                cls.daily_request, cls.last_api_daily_request = daily_request, datetime.now().timestamp()
            except AssertionError:  # If i can't recover those data
                pass

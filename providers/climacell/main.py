__all__ = ["ClimaCell"]

from requests import get as GET
from datetime import datetime, timezone, timedelta
from requests.exceptions import ConnectionError as RequestConnectionError
from requests.exceptions import ReadTimeout

from configs.providers.cc_config import API_KEY, PER_DAY_API_CURRENT_REQUESTS, PER_DAY_API_DAILY_REQUESTS, PER_DAY_API_HOURLY_REQUESTS, \
    PER_DAY_API_NOWCAST_REQUESTS, CUSTOM_WEATHER_ICON_CONDITIONS, PORT, DESC_PORT, HOUR_COUNT_PRP
from config import LAT, LON
from others.functions.press_at_alt import slp_to_zlp
from others.functions.get_weather_icon import get_weather_icon
from others.functions.deg_to_compass import deg_to_compass
from others.functions.wet_bulb import wet_bulb
from utilities.decorator_error_handler import retry


class ClimaCell(object):
    # CURRENT
    PER_DAY_API_CURRENT_REQUESTS = PER_DAY_API_CURRENT_REQUESTS
    current_request: dict = {}
    last_api_current_request: float = 0.0

    # NOWCAST
    PER_DAY_API_NOWCAST_REQUESTS = PER_DAY_API_NOWCAST_REQUESTS[0]
    nowcast_request: dict = {}
    last_api_nowcast_request: float = 0.0

    # HOURLY
    PER_DAY_API_HOURLY_REQUESTS = PER_DAY_API_HOURLY_REQUESTS
    hourly_request: dict = {}
    last_api_hourly_request: float = 0.0

    # DAILY
    PER_DAY_API_DAILY_REQUESTS = PER_DAY_API_DAILY_REQUESTS
    daily_request: dict = {}
    last_api_daily_request: float = 0.0

    @classmethod
    def get_current_conditions(cls) -> (str, str, str):
        cls.__api_request_current()

        return DESC_PORT[cls.current_request["weather_code"]["value"]][0], \
               DESC_PORT[cls.current_request["weather_code"]["value"]][1], \
               get_weather_icon(CUSTOM_WEATHER_ICON_CONDITIONS, cls.get_cloudiness(),
                                PORT[cls.current_request["weather_code"]["value"]],
                                "d" if cls.get_sunrise() <= datetime.now(cls.get_sunrise().tzinfo) < cls.get_sunset() else "n")

    @staticmethod
    def convert_from_ISO8601(datestring: str, ms: bool = True, tz: bool = True) -> datetime:
        if tz:
            return datetime.strptime(datestring, "%Y-%m-%dT%H:%M:%S.%fZ" if ms else "%Y-%m-%dT%H:%M:%SZ").replace(tzinfo=timezone.utc).astimezone(tz=None)
        return datetime.strptime(datestring, "%Y-%m-%dT%H:%M:%S.%fZ" if ms else "%Y-%m-%dT%H:%M:%SZ")

    @classmethod
    def get_current_time(cls) -> float:
        cls.__api_request_current()

        return cls.convert_from_ISO8601(cls.current_request["observation_time"]["value"], ).timestamp()

    @classmethod
    def get_visibility(cls) -> int:
        cls.__api_request_current()

        return cls.current_request["visibility"]["value"] * 1000  # Default km

    @classmethod
    def get_cloudiness(cls) -> int:
        cls.__api_request_current()

        return cls.current_request["cloud_cover"]["value"]

    @classmethod
    def get_sunrise(cls) -> datetime:
        cls.__api_request_current()

        return cls.convert_from_ISO8601(cls.current_request["sunrise"]["value"]) if cls.current_request.get("sunrise") else None

    @classmethod
    def get_sunset(cls) -> datetime:
        cls.__api_request_current()

        return cls.convert_from_ISO8601(cls.current_request["sunset"]["value"]) if cls.current_request.get("sunset") else None

    @classmethod
    def get_snow(cls) -> float:
        cls.__api_request_current()

        return cls.current_request["precipitation"]["value"] if cls.current_request["precipitation_type"]["value"] == "snow" else 0.00  # cm/h

    @classmethod
    def get_dt(cls) -> float:
        return (24 * 60 * 60) / cls.PER_DAY_API_CURRENT_REQUESTS

    @classmethod
    @retry((RequestConnectionError, ReadTimeout), tries=3, delay=0.5, jitter=1)
    def __api_request_current(cls) -> None:
        if (datetime.now().timestamp() - cls.last_api_current_request) >= cls.get_dt():
            url: str = "https://api.climacell.co/v3/weather/realtime"

            querystring: dict = {
                "lat": str(LAT),
                "lon": str(LON),
                "unit_system": "si",
                "fields": "sunrise,sunset,visibility,cloud_cover,weather_code,precipitation_type,precipitation",
                "apikey": API_KEY
            }

            r = GET(url, params=querystring)

            try:
                assert r.status_code == 200
                current_request: dict = r.json()

                cls.current_request, cls.last_api_current_request = current_request, datetime.now().timestamp()
            except AssertionError:  # If i can't recover those data
                pass

    @classmethod
    def snow_or_rain(cls, index: int) -> bool:
        """True: Snow, False: Rain"""
        for x in ["snow", "flurries"]:
            if x in cls.daily_request[index]["weather_code"]["value"].lower():
                return True
        return False

    @classmethod
    def get_daily_weather(cls, from_to: slice, hf: str) -> list:
        cls.__api_request_daily()

        return [
            {
                "when": datetime.strptime(day["observation_time"]["value"], "%Y-%m-%d").strftime("%A %d %B, %Y"),
                "temp_max": day["temp"][1]["max"]["value"],
                "temp_min": day["temp"][0]["min"]["value"],
                "weather": f'{DESC_PORT[day["weather_code"]["value"]][0]}, {DESC_PORT[day["weather_code"]["value"]][1].lower()}',
                "weather_icon_url": get_weather_icon(CUSTOM_WEATHER_ICON_CONDITIONS, 100, PORT[day["weather_code"]["value"]], "d"),
                "pop": day["precipitation_probability"]["value"],
                "rain": cls.accu_decision(datetime.strptime(day["observation_time"]["value"], "%Y-%m-%d"), day["precipitation_accumulation"]["value"], "rain", i, hf),
                "snow": cls.accu_decision(datetime.strptime(day["observation_time"]["value"], "%Y-%m-%d"), day["precipitation_accumulation"]["value"], "snow", i, hf),
                "wind_speed": day["wind_speed"][1]["max"]["value"] * 3.6,
                "wind_dir": deg_to_compass(day["wind_direction"][1]["max"]["value"]),
                "clouds": day["precipitation"][0]["max"]["value"]  # Precipitation rate in this case
            } for i, day in enumerate(cls.daily_request[from_to])
        ]

    @classmethod
    def get_daily_header(cls, day: int) -> str:
        if cls.daily_request:
            return f'↑ {cls.convert_from_ISO8601(cls.daily_request[day]["temp"][1]["observation_time"], ms=False).strftime("%d H%H")},  ↓ {cls.convert_from_ISO8601(cls.daily_request[day]["temp"][0]["observation_time"], ms=False).strftime("%d H%H")}'
        return ""

    @classmethod
    def get_dt_daily(cls) -> float:
        return (24 * 60 * 60) / cls.PER_DAY_API_DAILY_REQUESTS

    @classmethod
    @retry((RequestConnectionError, ReadTimeout), tries=3, delay=0.5, jitter=1)
    def __api_request_daily(cls) -> None:
        if (datetime.now().timestamp() - cls.last_api_daily_request) >= cls.get_dt_daily():
            url: str = "https://api.climacell.co/v3/weather/forecast/daily"

            querystring: dict = {
                "start_time": "now",
                "unit_system": "si",
                "lat": str(LAT),
                "lon": str(LON),
                "fields": "temp,weather_code,precipitation,wind_speed,wind_direction,precipitation_accumulation,precipitation_probability",
                "apikey": API_KEY
            }

            r = GET(url, params=querystring)

            try:
                assert r.status_code == 200
                daily_request: dict = r.json()

                cls.daily_request, cls.last_api_daily_request = daily_request, datetime.now().timestamp()
            except AssertionError:  # If i can't recover those data
                pass

    @classmethod
    def get_hourly_weather(cls) -> list:
        cls.__api_request_hourly()

        return [
            {
                "when": cls.convert_from_ISO8601(hour["observation_time"]["value"]).strftime("%A %d %B at %H:%S"),
                "temp": hour["temp"]["value"],
                "dew_point": hour["dewpoint"]["value"],
                "humidity": hour["humidity"]["value"],
                "visibility": hour["visibility"]["value"] * 1000,
                "pressure": slp_to_zlp(hour["baro_pressure"]["value"], hour["temp"]["value"] + 273.15),
                "weather": f'{DESC_PORT[hour["weather_code"]["value"]][0]}',
                "description": f'{DESC_PORT[hour["weather_code"]["value"]][1]}',
                "weather_icon_url": get_weather_icon(CUSTOM_WEATHER_ICON_CONDITIONS, hour["cloud_cover"]["value"],
                                                     PORT[hour["weather_code"]["value"]],
                                                     "d" if cls.convert_from_ISO8601(hour["sunrise"]["value"]) <= cls.convert_from_ISO8601(hour["observation_time"]["value"]) < cls.convert_from_ISO8601(hour["sunset"]["value"]) else "n"),
                "pop": hour["precipitation_probability"]["value"],
                "rain": hour["precipitation"]["value"] if hour["precipitation_type"]["value"] != "snow" else 0.0,
                "snow": hour["precipitation"]["value"] if hour["precipitation_type"]["value"] == "snow" else 0.0,
                "wind_speed": hour["wind_speed"]["value"] * 3.6,
                "wet_bulb": wet_bulb(hour["temp"]["value"], hour["humidity"]["value"], slp_to_zlp(hour["baro_pressure"]["value"], hour["temp"]["value"] + 273.15)),
                "wind_dir": deg_to_compass(hour["wind_direction"]["value"]),
                "clouds": hour["cloud_cover"]["value"]
            } for i, hour in enumerate(cls.hourly_request)
        ]

    @classmethod
    def get_dt_hourly(cls) -> float:
        return (24 * 60 * 60) / cls.PER_DAY_API_HOURLY_REQUESTS

    @classmethod
    @retry((RequestConnectionError, ReadTimeout), tries=3, delay=0.5, jitter=1)
    def __api_request_hourly(cls) -> None:
        if (datetime.now().timestamp() - cls.last_api_hourly_request) >= cls.get_dt_hourly():
            url: str = "https://api.climacell.co/v3/weather/forecast/hourly"

            querystring: dict = {
                "start_time": "now",
                "unit_system": "si",
                "lat": str(LAT),
                "lon": str(LON),
                "fields": "sunrise,sunset,temp,humidity,weather_code,precipitation_probability,wind_speed,dewpoint,cloud_cover,wind_direction,baro_pressure,precipitation,precipitation_type,visibility",
                "apikey": API_KEY
            }

            r = GET(url, params=querystring)

            try:
                assert r.status_code == 200
                hourly_request: dict = r.json()

                cls.hourly_request, cls.last_api_hourly_request = hourly_request, datetime.now().timestamp()
            except AssertionError:  # If i can't recover those data
                pass

    @classmethod
    def get_precipitation(cls, hf: str) -> (str, float, str):
        if hf == "ClimaCell":
            cls.__api_request_hourly()
        cls.__api_request_nowcast()

        if cls.nowcast_request[0]["precipitation"]["value"]:
            for i, prep in enumerate(cls.nowcast_request):
                if not prep["precipitation"]["value"]:
                    if i > 1:
                        prep_message: str = f"Precipitation will end within {i} minutes"
                    else:
                        prep_message: str = f"Precipitation will end within {i} minute"
                    prep_flag: str = "minutecc"
                    cls.PER_DAY_API_NOWCAST_REQUESTS = PER_DAY_API_NOWCAST_REQUESTS[1]
                    break
            else:
                prep_flag: str = "minutecc"
                prep_message: str = f"Precipitation won't end within 6 hours"
                cls.PER_DAY_API_NOWCAST_REQUESTS = PER_DAY_API_NOWCAST_REQUESTS[0]
        else:
            for i, prep in enumerate(cls.nowcast_request):
                if prep["precipitation"]["value"]:
                    if i > 1:
                        prep_message: str = f"Precipitation expected within {i} minutes"
                    else:
                        prep_message: str = f"Precipitation expected within {i} minute"
                    prep_flag: str = "minutecc"
                    cls.PER_DAY_API_NOWCAST_REQUESTS = PER_DAY_API_NOWCAST_REQUESTS[1]
                    break
            else:
                if hf == "ClimaCell":
                    for i, prep in enumerate(cls.hourly_request[1:]):
                        if prep["precipitation"]["value"]:
                            if i + 1 > 1:
                                prep_message: str = f"Precipitation expected in {i + 1} hours"
                            else:
                                prep_message: str = f"Precipitation expected in {i + 1} hour"
                            prep_flag: str = "hourcc"
                            cls.PER_DAY_API_NOWCAST_REQUESTS = PER_DAY_API_NOWCAST_REQUESTS[0]
                            break
                    else:
                        prep_flag: str = "hourcc"
                        prep_message: str = f"No precipitation expected in 108 hours"
                        cls.PER_DAY_API_NOWCAST_REQUESTS = PER_DAY_API_NOWCAST_REQUESTS[0]
                else:
                    prep_flag: str = "minutecc"
                    prep_message: str = f"No precipitation expected in 6 hours"
                    cls.PER_DAY_API_NOWCAST_REQUESTS = PER_DAY_API_NOWCAST_REQUESTS[0]

        if prep_flag == "hourcc":
            precip: float = sum([x["precipitation"]["value"] for x in cls.hourly_request])
        else:
            precip: float = sum(list(map(lambda x: (x["precipitation"]["value"] / 60) if x["precipitation"]["value"] else 0.0, cls.nowcast_request)))

        return prep_message, precip, prep_flag

    @classmethod
    def get_dt_nowcast(cls) -> float:
        return (24 * 60 * 60) / cls.PER_DAY_API_NOWCAST_REQUESTS

    @classmethod
    @retry((RequestConnectionError, ReadTimeout), tries=3, delay=0.5, jitter=1)
    def __api_request_nowcast(cls) -> None:
        if (datetime.now().timestamp() - cls.last_api_nowcast_request) >= cls.get_dt_nowcast():
            url: str = "https://api.climacell.co/v3/weather/nowcast"

            querystring: dict = {
                "start_time": "now",
                "unit_system": "si",
                "timestep": "1",
                "lat": str(LAT),
                "lon": str(LON),
                "fields": "precipitation",
                "apikey": API_KEY
            }

            r = GET(url, params=querystring)

            try:
                assert r.status_code == 200
                nowcast_request: dict = r.json()

                cls.nowcast_request, cls.last_api_nowcast_request = nowcast_request, datetime.now().timestamp()
            except AssertionError:  # If i can't recover those data
                pass

    @classmethod
    def accu_decision(cls, data: datetime, value: float, _type: str, i: int, hf: str) -> float:
        if hf == "ClimaCell" and HOUR_COUNT_PRP and data.replace(tzinfo=timezone.utc) <= datetime.now(tz=timezone.utc) + timedelta(days=3):
            cls.__api_request_hourly()
            start_time: datetime = data + timedelta(hours=6)
            end_time: datetime = data + timedelta(days=1, hours=7)

            if _type == "snow":
                return sum([x["precipitation"]["value"] if (x["precipitation_type"]["value"].lower() in ["snow", "flurries"] and start_time <= cls.convert_from_ISO8601(x["observation_time"]["value"], tz=False) <= end_time) else .0 for x in cls.hourly_request])
            else:
                return sum([x["precipitation"]["value"] if (x["precipitation_type"]["value"].lower() not in ["snow", "flurries"] and start_time <= cls.convert_from_ISO8601(x["observation_time"]["value"], tz=False) <= end_time) else .0 for x in cls.hourly_request])
        else:
            return value if cls.snow_or_rain(i) and _type == "snow" else (value if not cls.snow_or_rain(i) and _type == "rain" else 0.0)

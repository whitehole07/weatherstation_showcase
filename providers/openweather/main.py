from requests import get as GET
from datetime import datetime
from requests.exceptions import ConnectionError as RequestConnectionError
from requests.exceptions import ReadTimeout

from configs.providers.ow_config import API_KEY, PER_DAY_API_CURRENT_REQUESTS, CUSTOM_WEATHER_ICON_CONDITIONS
from config import LAT, LON
from others.functions.get_weather_icon import get_weather_icon
from others.functions.deg_to_compass import deg_to_compass
from others.functions.wet_bulb import wet_bulb
from others.functions.press_at_alt import slp_to_zlp
from utilities.decorator_error_handler import retry


class OpenWeather(object):
    PER_DAY_API_CURRENT_REQUESTS = PER_DAY_API_CURRENT_REQUESTS[0]
    current_request: dict = {}
    last_api_request: float = 0.0

    @classmethod
    def get_current_conditions(cls) -> (str, str, str):
        cls.__api_request_current()

        return cls.current_request["current"]["weather"][0]["main"].capitalize(), \
               cls.current_request["current"]["weather"][0]["description"].capitalize(), \
               get_weather_icon(CUSTOM_WEATHER_ICON_CONDITIONS, cls.get_cloudiness(),
                                cls.current_request["current"]["weather"][0]["id"],
                                cls.current_request["current"]["weather"][0]["icon"][-1])

    @classmethod
    def get_current_time(cls) -> float:
        cls.__api_request_current()

        return cls.current_request["current"]["dt"]

    @classmethod
    def get_visibility(cls) -> int:
        cls.__api_request_current()

        return cls.current_request["current"]["visibility"]

    @classmethod
    def get_cloudiness(cls) -> int:
        cls.__api_request_current()

        return cls.current_request["current"]["clouds"]

    @classmethod
    def get_sunrise(cls) -> datetime:
        cls.__api_request_current()

        return datetime.fromtimestamp(cls.current_request["current"]["sunrise"]) if cls.current_request.get("current") else None

    @classmethod
    def get_sunset(cls) -> datetime:
        cls.__api_request_current()

        return datetime.fromtimestamp(cls.current_request["current"]["sunset"]) if cls.current_request.get("current") else None

    @classmethod
    def get_snow(cls) -> float:
        cls.__api_request_current()

        return cls.current_request["current"]["snow"]["1h"] if cls.current_request["current"].get("snow") and cls.current_request["current"]["snow"].get("1h") else 0.00  # cm/h

    @classmethod
    def get_daily_weather(cls, from_to: slice, *args) -> list:
        cls.__api_request_current()

        if not cls.current_request.get("daily"):
            return []

        return [
            {
                "when": datetime.fromtimestamp(day["dt"]).strftime("%A %d %B, %Y"),
                "temp_max": day["temp"]["max"],
                "temp_min": day["temp"]["min"],
                "weather": f'{day["weather"][0]["main"].capitalize()}, {day["weather"][0]["description"]}',
                "weather_icon_url": get_weather_icon(CUSTOM_WEATHER_ICON_CONDITIONS, day["clouds"],
                                                     day["weather"][0]["id"], day["weather"][0]["icon"][-1]),
                "pop": day["pop"] * 100,
                "rain": day["rain"] if day.get("rain") else 0.0,
                "snow": day["snow"] if day.get("snow") else 0.0,
                "wind_speed": day["wind_speed"] * 3.6,
                "wind_dir": deg_to_compass(day["wind_deg"]),
                "clouds": day["clouds"]
            } for i, day in enumerate(cls.current_request["daily"][from_to])
        ]

    @classmethod
    def get_daily_header(cls, _: int) -> str:
        return ""  # Not implemented yet

    @classmethod
    def get_hourly_weather(cls) -> list:
        cls.__api_request_current()

        if not cls.current_request.get("hourly"):
            return []

        return [
            {
                "when": datetime.fromtimestamp(hour["dt"]).strftime("%A %d %B at %H:%S"),
                "temp": hour["temp"],
                "dew_point": hour["dew_point"],
                "humidity": hour["humidity"],
                "visibility": hour["visibility"],
                "pressure": slp_to_zlp(hour["pressure"], hour["temp"] + 273.15),
                "weather": f'{hour["weather"][0]["main"].capitalize()}',
                "description": f'{hour["weather"][0]["description"].capitalize()}',
                "weather_icon_url": get_weather_icon(CUSTOM_WEATHER_ICON_CONDITIONS, hour["clouds"],
                                                     hour["weather"][0]["id"], hour["weather"][0]["icon"][-1]),
                "pop": hour["pop"] * 100,
                "rain": hour["rain"]["1h"] if hour.get("rain") and hour["rain"].get("1h") else 0.0,
                "snow": hour["snow"]["1h"] if hour.get("snow") and hour["snow"].get("1h") else 0.0,
                "wind_speed": hour["wind_speed"] * 3.6,
                "wet_bulb": wet_bulb(hour["temp"], hour["humidity"], slp_to_zlp(hour["pressure"], hour["temp"] + 273.15)),
                "wind_dir": deg_to_compass(hour["wind_deg"]),
                "clouds": hour["clouds"]
            } for i, hour in enumerate(cls.current_request["hourly"])
        ]

    @classmethod
    def get_precipitation(cls, *args) -> (str, float, str):
        cls.__api_request_current()

        if not cls.current_request.get("minutely"):
            return f"Unable to retrieve minutely forecast", 0.0, "hour"

        if cls.current_request["minutely"][0]["precipitation"]:
            for i, prep in enumerate(cls.current_request["minutely"]):
                if not prep["precipitation"]:
                    if i > 1:
                        prep_message: str = f"Precipitation will end within {i} minutes"
                    else:
                        prep_message: str = f"Precipitation will end within {i} minute"
                    prep_flag: str = "minute"
                    cls.PER_DAY_API_CURRENT_REQUESTS = PER_DAY_API_CURRENT_REQUESTS[1]
                    break
            else:
                prep_flag: str = "minute"
                prep_message: str = f"Precipitation won't end within an hour"
                cls.PER_DAY_API_CURRENT_REQUESTS = PER_DAY_API_CURRENT_REQUESTS[0]
        else:
            for i, prep in enumerate(cls.current_request["minutely"]):
                if prep["precipitation"]:
                    if i > 1:
                        prep_message: str = f"Precipitation expected within {i} minutes"
                    else:
                        prep_message: str = f"Precipitation expected within {i} minute"
                    prep_flag: str = "minute"
                    cls.PER_DAY_API_CURRENT_REQUESTS = PER_DAY_API_CURRENT_REQUESTS[1]
                    break
            else:
                for i, prep in enumerate(cls.current_request["hourly"][1:]):
                    if prep.get("rain"):
                        if i + 1 > 1:
                            prep_message: str = f"Precipitation expected in {i + 1} hours"
                        else:
                            prep_message: str = f"Precipitation expected in {i + 1} hour"
                        prep_flag: str = "hour"
                        cls.PER_DAY_API_CURRENT_REQUESTS = PER_DAY_API_CURRENT_REQUESTS[0]
                        break
                else:
                    prep_flag: str = "hour"
                    prep_message: str = f"No precipitation expected within 48 hours"
                    cls.PER_DAY_API_CURRENT_REQUESTS = PER_DAY_API_CURRENT_REQUESTS[0]

        if prep_flag == "hour":
            precip: float = sum(
                list(map(lambda x: x["rain"]["1h"] if x.get("rain") and x["rain"].get("1h") else 0.0, cls.current_request["hourly"])))
        else:
            precip: float = sum(list(map(lambda x: x["precipitation"] / 60, cls.current_request["minutely"])))

        return prep_message, precip, prep_flag

    @classmethod
    def get_dt(cls) -> float:
        return (24 * 60 * 60) / cls.PER_DAY_API_CURRENT_REQUESTS

    @classmethod
    @retry((RequestConnectionError, ReadTimeout), tries=3, delay=0.5, jitter=1)
    def __api_request_current(cls) -> None:
        if (datetime.now().timestamp() - cls.last_api_request) >= cls.get_dt():
            r = GET(f"http://api.openweathermap.org/data/2.5/onecall?lat={LAT}&lon={LON}&exclude=alerts&appid={API_KEY}&units=metric",
                    timeout=10)

            try:
                assert r.status_code == 200
                current_request: dict = r.json()

                assert current_request.get("current") and current_request.get("hourly") and current_request.get("daily")

                cls.current_request, cls.last_api_request = current_request, datetime.now().timestamp()
            except AssertionError:  # If i can't recover those data
                pass

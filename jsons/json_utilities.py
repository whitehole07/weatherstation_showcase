import json
from config import GLOBAL_PATH


class JSONUtilities:
    @staticmethod
    def load_json(relative_path: str) -> dict:
        """It loads the JSON from the relative path"""
        with open(f"{GLOBAL_PATH}/jsons/json/{relative_path}", "r") as lg:
            return json.loads(lg.read())

    @staticmethod
    def save_json(relative_path: str, save_dict: dict) -> None:
        """It saves the JSON from the relative path"""
        with open(f"{GLOBAL_PATH}/jsons/json/{relative_path}", "w") as lg:
            json.dump(save_dict, lg, ensure_ascii=False, indent=2)
